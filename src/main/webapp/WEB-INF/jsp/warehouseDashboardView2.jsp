<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>全日物流宅配訂單管理系統</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="http://testotms.gogoffcc.com/mtms_cdnv2/images/favicon.ico" />
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
.bar{
	margin:0;
	width:100%;
	height:50px;
	background-color:#164f55
}
.bar span{
	display:inline-block;
	color:white;
	margin:10px 50px;
}
.content{
	margin:70px 1px 10px 1px;
	width:auto;
	height:100%;
	border-radius:5px;
	border:1px solid #a0c5cf;
	overflow-x:scroll;
	display:flex;
	display:flex-wrap;
	align-content:center;
}

.content .detail{
	display:inline-block;
	font-size:15px;
	width:20%;
	allign-content
}
.content .detail table{
	display:inline-table;
	width:100%;
	font-size:20px;
	text-align:center;
}
.content .detail td{
	width:auto;
	height:70px;
	border:1px solid #005f89;
	border-collapse:collapse
}
.content .detail th{
	width:auto;
	border:1px solid #005f89;
	border-collapse:collapse
}
.content .detail .outer-box{
	width:98%;
	height:auto;
	margin:auto;
	border: 1px solid #005f89;
	border-radius: 3px;
	font-size:15px;
	color:#363636;
}
.content .detail .line-gray{
	background-color:#cccccc;
	height:15px;
	width:95%;
	margin:auto;
	margin-bottom:2px;
}
</style>
<body style="background-color:#e1ebec;color:black">
	<!-- Top container -->
	<div class="bar">
		<span style="font-size:20px;margin:10px 10px 10px 50px"><i class="fa fa-dashboard"></i></span>
		<span style="font-size:20px;margin:10px 0px"><b>全日物流倉管作業看板</b></span>
		<span style="font-size:20px;margin:10px 50px 10px 10px;float:right;"><b>${{dcDisName}}倉管</b></span>
		<span style="font-size:20px;margin:10px 0px;float:right;"><i class="fa fa-user"></i></span>
	</div>
	<span style="font-size:20px;margin:10px 50px 10px 10px;float:right;"><b>更新時間 ： </b></span>
	<div class="content">
		<div class="detail">
			<!-- main table -->
			<table>
				<tr style="background-color:teal;">
					<th colspan="2" style="color:white">集貨回倉</th>
				</tr>
				<tr>
					<th>已回倉</th>
					<td>${{finishCollect}}箱</td>
					
				</tr>
				<tr>
					<th>應回倉</th>
					<td>${{totalCollect}}箱</td>
				</tr>
			</table>
			<!-- detail table -->
			<div class="outer-box" style="margin-top:80px;">
				<span style="font-size:20px;text-align:left;margin:5px;"><b> 配送地 ： </b></span>
				<div style="margin-bottom:5px;margin-top:10px;margin-collapse:collapse">
					<span style="text-align:left;margin:3px;">五股 ： </span>
					<span style="float:right;">10</span>
					<div class="line-gray">
						<div style="height:15px;width:80%;background-color:orange;display:inline-block;"></div>
						<span>80%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">觀音 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">晶富 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">虎尾 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:100%;background-color:green;display:inline-block;"></div>
						<span></span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">仁武 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
			</div>
		</div>
		<div class="detail">
			<!-- main table -->
			<table>
				<tr style="background-color:teal;">
					<th colspan="2" style="color:white">在倉發貨</th>
				</tr>
				<tr>
					<th>已理貨</th>
					<td>${{finishPackOut}}箱</td>
					
				</tr>
				<tr>
					<th>應理貨</th>
					<td>${{totalPackOut}}箱</td>
				</tr>
			</table>
			<!-- detail table -->
			<div class="outer-box" style="margin-top:80px;">
				<span style="font-size:20px;text-align:left;margin:5px;"><b> 配送地 ： </b></span>
				<div style="margin-bottom:5px;margin-top:10px;margin-collapse:collapse">
					<span style="text-align:left;margin:3px;">五股 ： </span>
					<span style="float:right;">10</span>
					<div class="line-gray">
						<div style="height:15px;width:80%;background-color:orange;display:inline-block;"></div>
						<span>80%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">觀音 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">晶富 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">虎尾 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:100%;background-color:green;display:inline-block;"></div>
						<span></span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">仁武 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
			</div>
		</div>
		<div class="detail">
			<!-- main table -->
			<table>
				<tr style="background-color:#067eb9;">
					<th colspan="2" style="color:white">抵達分貨中心</th>
				</tr>
				<tr>
					<th>已抵達分貨中心</th>
					<td>${{finishTransferIn}}箱</td>
					
				</tr>
				<tr>
					<th>應抵達分貨中心</th>
					<td>${{totalTransferIn}}箱</td>
				</tr>
			</table>
			<!-- detail table -->
			<div class="outer-box" style="margin-top:80px;">
				<span style="font-size:20px;text-align:left;margin:5px;"><b> 出貨地 ： </b></span>
				<div style="margin-bottom:5px;margin-top:10px;margin-collapse:collapse">
					<span style="text-align:left;margin:3px;">五股 ： </span>
					<span style="float:right;">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">觀音 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">晶富 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">虎尾 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">仁武 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
			</div>
		</div>
		<div class="detail">
			<!-- main table -->
			<table>
				<tr style="background-color:#0c8b68;">
					<th colspan="2" style="color:white">轉運裝車</th>
				</tr>
				<tr>
					<th>已裝車</th>
					<td>${{finishTransferOut}}箱</td>
					
				</tr>
				<tr>
					<th>應裝車</th>
					<td>${{totalTransferOut}}箱</td>
				</tr>
			</table>
			<!-- detail table -->
			<div class="outer-box" style="margin-top:80px;">
				<span style="font-size:20px;text-align:left;margin:5px;"><b> 配送地 ： </b></span>
				<div style="margin-bottom:5px;margin-top:10px;margin-collapse:collapse">
					<span style="text-align:left;margin:3px;">五股 ： </span>
					<span style="float:right;">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">觀音 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">晶富 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">虎尾 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
				<div style="margin-bottom:10px;">
					<span style="text-align:left;margin:3px;">仁武 ： </span>
					<span style="float:right">10</span>
					<div class="line-gray">
						<div style="height:15px;width:25%;background-color:red;display:inline-block;"></div>
						<span>25%</span>
					</div>
				</div>
			</div>
		</div>
		<div class="detail">
			<!-- main table -->
			<table>
				<tr style="background-color:#0c8b68;">
					<th colspan="2" style="color:white">配送</th>
				</tr>
				<tr>
					<th>配送完成</th>
					<td>${{successDelivery}}箱</td>
				</tr>
				<tr>
					<th>配送異常</th>
					<td>${{failDelivery}}箱</td>
				</tr>
				<tr>
					<th>應配送</th>
					<td>${{totalDelivery}}箱</td>
				</tr>
			</table>
			<!-- detail table -->
			<table>
				<tr style="background-color:#067eb9;">
					<th colspan="2" style="color:white">配後回倉</th>
				</tr>
				<tr>
					<th>包裹完好</th>
					<td>${{goodBox}}箱</td>
				</tr>
				<tr>
					<th>包裹異常</th>
					<td>${{brokenBox}}箱</td>
				</tr>
				<tr>
					<th>應回倉</th>
					<td>${{totalReturnBox}}箱</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
	