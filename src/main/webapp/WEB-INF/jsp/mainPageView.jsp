<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>全日物流宅配訂單管理系統</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="shortcut icon" href="http://testotms.gogoffcc.com/mtms_cdnv2/images/favicon.ico" />
<style>
.main-container{
	margin: 100px auto;
	width: 70%;
	height: auto;
	background-color: #f4f5f5;
	padding: 10px;
	border-radius:10px;
}
.div-center{
	margin: auto;
  	width: auto;
  	text-align:center;
}
.main-container-content{
	color:#164f55;
	font-family:verdana;
	font-size:50px;
	padding: 5px;
	text-align:center;
}
.logo-container{
	display: block;
	margin-left: auto;
	margin-right: auto;
	margin-top : 60px;
}

.main-container button {
	-webkit-appearance: button;
    -moz-appearance: button;
    appearance: button;
	background-color: #4a91bb;
	border: none;
	color: white!important;
	padding: 16px 10px;
	margin: 5px 10px;
	text-align: center;
	font-size: 35px;
	transition-duration: 0.4s;
	cursor: pointer;
	border-radius:4px;
	width:20%;
	
}
.main-container button:hover{
	background-color: #004581;
  	color: white;
}
</style>
</head>
<body style="background-color: #015768">
	<div class="main-container">
		<img src="./resources/img/logo.png" alt="Roundday logo" class="logo-container">
		<p class="main-container-content">全日物流倉管作業看板</p>
		<hr style="border-top: 3px solid #e7f0ed;">
		<p class="main-container-content" style="font-size:30px"><b>請選配送中心 :</b></p>
		<div class="div-center" id=dcs>
			${dcInput}
		</div>
		
	</div>
</body>
</html>
