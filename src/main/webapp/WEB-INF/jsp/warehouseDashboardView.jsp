<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import = "mtmsDashboard.utils.DateUtils" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<title>全日物流宅配訂單管理系統</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="https://t-oms.roundday.com.tw/mtms_cdnv2/images/favicon.ico" />
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
.bar{
	margin:0;
	width:100%;
	height:70px;
	background-color:#164f55
}
.bar span{
	display:inline-block;
	color:white;
	margin:10px 50px;
}
.content-box{
	margin:70px 1px 10px 1px;
	width:auto;
	height:100%;
	border-radius:5px;
	border:1px solid #a0c5cf;
}
.content{
	width:100%;
	height:100%;
	display:flex;
	display:flex-wrap;
	align-content:center;
}
.content .main{
	display:inline-block;
	font-size:largepx;
	width:20%;
	allign-content : center;
}
.content .detail{
	display:inline-block;
	font-size:largepx;
	width:20%;
	allign-content : center;
}
.content .main table{
	display:inline-table;
	width:100%;
	text-align:center;
	font-size:50px;
}
.content .main td{
	width:auto;
	height:70px;
	border:1px solid #005f89;
	border-collapse:collapse;
	font-size:50px;
}
.content .main th{
	width:auto;
	border:1px solid #005f89;
	border-collapse:collapse;
	font-size:50px;
}
.content .detail .inner-box{
	width:98%;
	height:100%;
	margin:auto;
	border: 1px solid #005f89;
	border-radius: 3px;
	font-size:15px;
	color:#363636;
}
.content .detail .line-gray{
	background-color:#cccccc;
	height:30px;
	width:95%;
	margin:auto;
	margin-bottom:2px;
}
</style>
<body style="background-color:#e1ebec;color:black">
	<!-- Top container -->
	<% //response.setIntHeader("Refresh", 60);%>
	<div class="bar">
		<span style="font-size:30px;margin:10px 10px 10px 50px"><i class="fa fa-dashboard"></i></span>
		<span style="font-size:30px;margin:10px 0px"><b>全日物流倉管作業看板</b></span>
		<span id="dcDisName" style="font-size:30px;margin:10px 50px 10px 10px;float:right;"><b>${dcDisName}倉管</b></span>
		<span style="font-size:30px;margin:10px 0px;float:right;"><i class="fa fa-user"></i></span>
	</div>
	<div style='margin:10px auto;font-size:50px'>
		<span><b>${dcDisName}物流中心</b></span>
		<b><%out.print(DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT));%></b>
		<input id="dcId" type="hidden" value="<%=request.getParameter("dc")%>"></input>
		<!--  <span><b>
		<%//
		//if(request.getParameter("date")==null){
		//	out.print(DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT));
		//}else{
		//	out.print(DateUtils.toString(DateUtils.parseStringToDate(request.getParameter("date"), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT));
		//}
		%></b></span>-->
		<span style='float:right;font-size:50px' id="date"><b>更新時間 ： </b></span>
		<!-- <span style='float:right;font-size:50px'><b>更新時間 ： 
		<%
		//if(request.getParameter("date")==null){
		//	out.print(DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.TIME_FORMAT));
		//}else{
		//	out.print(DateUtils.toString(DateUtils.parseStringToDate(request.getParameter("date").toString()+"235959", DateUtils.DATE_TIME_FORMAT_14), DateUtils.TIME_FORMAT));
		//}
	    
		%>
		</b></span>-->
	</div>
<!-- 	<form style='display:inline' action='../warehouseDashboardTest/?dc="<%=request.getParameter("dc")%>"&date=' method = 'GET'>
		<label><b>查詢日期 :</b></label>
	    <input type="text" name="date">
	    <input type="hidden" name="dc" value="<%=request.getParameter("dc")%>">
		<button type='submit'>查詢</button>
	</form> -->
	<div class="content-box">
		<div class="content">
			<div class="main">
				<table>
					<tr style="background-color:#005f89;">
						<th colspan="2" style="color:white">在倉發貨</th>
					</tr>
					<tr>
						<th colspan="2" style='font-size:20px'><b>重置時間 : 00:00:00</b></th>
					</tr>
					<tr>
						<th>應理貨</th>
						<th>已理貨</th>
					</tr>
					<tr>
						<td rowspan="2" id="totalLeaveWarehouse">${totalLeaveWarehouse}單</td>
						<td id="leaveWarehouse">${leaveWarehouse}單</td>
					</tr>
					<tr>
						<td id="leaveWarehouseBox">${leaveWarehouseBox}箱</td>
					</tr>
				</table>
			</div>
			<div class="main">	
				<table>
					<tr style="background-color:#005f89;">
						<th colspan="2" style="color:white">集貨回倉</th>
					</tr>
					<tr>
						<th colspan="2" style='font-size:20px'><b>重置時間 : 00:00:00</b></th>
					</tr>
					<tr>
						<th>應回倉</th>
						<th>已回倉</th>
					</tr>
					<tr>
						<td id="totalCollect">${totalCollect}箱</td>
						<td id="finishCollect">${finishCollect}箱</td>
					</tr>
				</table>
			</div>
			<div class="main">
				<table>
					<tr style="background-color:#005f89;">
						<th colspan="2" style="color:white">轉運裝車</th>
					</tr>
					<tr>
						<th colspan="2" style='font-size:20px'><b>重置時間 : 09:00:00</b></th>
					</tr>
					<tr>
						<th>應裝車</th>
						<th>已裝車</th>
					</tr>
					<tr>
						<td id="transferOut">${transferOut}箱</td>
						<td id="leaveHub">${leaveHub}箱</td>
					</tr>
				</table>
			</div>
			<div class="main">
				<table>
					<tr style="background-color:#005f89;">
						<th colspan="2" style="color:white">轉運到倉</th>
					</tr>
					<tr>
						<th colspan="2" style='font-size:20px'><b>重置時間 : 12:00:00</b></th>
					</tr>
					<tr>
						<th>應抵達</th>
						<th>已抵達</th>
					</tr>
					<tr>
						<td id="transferIn">${transferIn}箱</td>
						<td id="arriverHub">${arriveHub}箱</td>
					</tr>
				</table>
			</div>
			<div class="main">
				<table>
					<tr style="background-color:#005f89;">
						<th colspan="2" style="color:white">配送</th>
					</tr>
					<tr>
						<th colspan="2" style='font-size:20px'><b>重置時間 : 00:00:00</b></th>
					</tr>
					<tr>
						<th>應配送</th>
						<th>配送完成</th>
					</tr>
					<tr>
						<td rowspan="3" id="totalDelivery">${totalDelivery}箱</td>
						<td id="successDelivery">${successDelivery}箱</td>
						
					</tr>
					<tr>
						<th>配送異常</th>
					</tr>
					<tr>
						<td id="failDelivery">${failDelivery}箱</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="content">
			<div class="detail">
				<div class="inner-box">
					<span style="font-size:30px;text-align:left;margin:3px;"><b>配送地 ：</b> </span>
					${leaveWarehouseDetail}
					<div style='margin-bottom:10px' id="leaveWarehouseDetail">
					</div>
				</div>
			</div>
			<div class="detail">
				<div class="inner-box">
					<span style="font-size:30px;text-align:left;margin:3px;"><b>配送地 ：</b></span>
					${collectDetail}
					<div style='margin-bottom:10px' id="collectDetail">
					</div>
				</div>
			</div>
			<div class="detail">
				<div class="inner-box">
					<span style="font-size:30px;text-align:left;margin:3px;"><b>配送地 ： </b></span>
					${transferOutDetail}
					<div style='margin-bottom:10px' id="transferOutDetail">
					</div>
				</div>
			</div>
			<div class="detail">
				<div class="inner-box">
					<span style="font-size:30px;text-align:left;margin:3px;"><b>出貨地 ： </b></span>
					${transferInDetail}
					<div style='margin-bottom:10px' id="transferInDetail">
					</div>
				</div>
			</div>
			<div class="main">
				<table style="margin:auto">
				<tr style="background-color:#005f89;">
					<th colspan="2" style="color:white">配後回倉</th>
				</tr>
				<tr>
					<th>應回倉</th>
					<th>已回倉</th>
				</tr>
				<tr>
					<td id="totalReturn">${totalReturn}箱</td>
					<td id="finishReturn">${finishReturn}箱</td>
				</tr>
			</table>
			</div>
		</div>
	</div>
	
	<!--"./resources/img/logo.png"-->
	<script type="text/javascript" src="../resources/js/warehouseDashboard.js"></script>
</body>
</html>
	