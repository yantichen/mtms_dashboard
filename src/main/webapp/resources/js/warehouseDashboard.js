/**
 * 
 */

var dcId = ""
var module={
		
	getWarehouseOutbound : function(){
		var url = '../warehouseOutbound/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#totalLeaveWarehouse').html(datas.total+"單");
				$('#leaveWarehouse').html(datas.finish+"單");
				$('#leaveWarehouseBox').html(datas.finishBox+"箱");
				$('#leaveWarehouseDetail').html(datas.detail)
			}
		});
	},
	getCollectOrder : function(){
		var url = '../collectOrder/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#totalCollect').html(datas.total+"箱");
				$('#finishCollect').html(datas.finish+"箱");
				$('#collectDetail').html(datas.detail);
			}
		});
	},
	getTransferOut : function(){
		var url = '../transferOut/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#transferOut').html(datas.total+"箱");
				$('#leaveHub').html(datas.finish+"箱");
				$('#transferOutDetail').html(datas.detail);
			}
		});
	},
	getTransferIn : function(){
		var url = '../transferIn/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#transferIn').html(datas.total+"箱");
				$('#arriverHub').html(datas.finish+"箱");
				$('#transferInDetail').html(datas.detail);
			}
		});
	},
	getDelivery : function(){
		var url = '../delivery/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#totalDelivery').html(datas.total+"箱");
				$('#successDelivery').html(datas.success+"箱");
				$('#failDelivery').html(datas.fail+"箱");
			}
		});
	},
	getReturnWarehouse : function(){
		var url = '../returnWarehouse/?dc='+dcId
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#totalReturn').html(datas.total+"箱");
				$('#finishReturn').html(datas.finish+"箱");
			}
		});
	},
	date : function(){
		var url = '../date/'
		return $.ajax({
			url: url,
			success : function(datas) {
				$('#date').html("<b>更新時間 ： "+datas+"</b>");
			}
		});
	},
}

$(document).ready(function(){
	dcId = $('#dcId').val();
	module.date()
	setInterval(function(){
	module.date()},300000)
	
	module.getWarehouseOutbound()
	setInterval(function(){
	module.getWarehouseOutbound()},300000)
	
	module.getCollectOrder()
	setInterval(function(){
	module.getCollectOrder()},300000)
	
	module.getTransferIn()
	setInterval(function(){
	module.getTransferIn()},300000)
	
	module.getTransferOut()
	setInterval(function(){
	module.getTransferOut()},300000)
	
	module.getDelivery()
	setInterval(function(){
	module.getDelivery()},300000)
	
	module.getReturnWarehouse()
	setInterval(function(){
	module.getReturnWarehouse()},300000)
})


