package mtmsDashboard.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import jodd.util.ArraysUtil;
import mtmsDashboard.common.CommonDao;
import mtmsDashboard.common.Pa;
import mtmsDashboard.model.Dc;
import mtmsDashboard.utils.SystemOutPrintHandler;

@Service
public class DashboardDao extends CommonDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Map<String,Object>> getCollectOrder(String targetDc, String sourceDc, String st, String et, String[] flows, String[] finishFlows, String[] excludeFlows){
		StringBuilder sql = new StringBuilder("SELECT sum(oms_ec_doc.boxQty) as totalBox, sum(finish.fin*oms_ec_doc.boxQty) as finishBox, sourceDcId, targetDcId "
				+"FROM oms_ec_order_flow_history JOIN oms_ec_dc_transfer ON oms_ec_order_flow_history.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"JOIN oms_ec_doc ON oms_ec_order_flow_history.ecDocId = oms_ec_doc.ecDocId "
				+"left JOIN (SELECT max(oms_ec_order_flow_history.updateDate) as exc, ecDocId from oms_ec_order_flow_history where flow in "+getValuesSqlMarkString(excludeFlows.length)+" group by ecDocId) "
				+"exclude on oms_ec_order_flow_history.ecDocId= exclude.ecDocId "
				+"left JOIN (SELECT count(oms_ec_order_flow_history.ecDocId) as exc, ecDocId from oms_ec_order_flow_history where flow = '"+Pa.DOC_FLOW_START_OUTBOUNT+"' group by ecDocId) "
				+"warehouse on oms_ec_order_flow_history.ecDocId= warehouse.ecDocId "
				+"left JOIN (SELECT count(distinct oms_ec_order_flow_history.ecDocId) as fin, ecDocId from oms_ec_order_flow_history where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(finishFlows.length)+" "
				+"and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate<= ? group by ecDocId) "
				+"finish on oms_ec_order_flow_history.ecDocId= finish.ecDocId "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(flows.length)+" and warehouse.exc is null "
				+"and oms_ec_order_flow_history.updateDate >= ? "
				+"and oms_ec_order_flow_history.updateDate<= ? "
				+"and (exclude.exc is null or exclude.exc>oms_ec_order_flow_history.updateDate) ");
		if(StringUtils.isNotBlank(targetDc)) {
			sql.append(" and targetDcId =  '" + targetDc + "'");
		}
		if(StringUtils.isNotBlank(sourceDc)) {
			sql.append(" and sourceDcId = '" + sourceDc +"'");
		}
		sql.append(" group by sourceDcId,targetDcId");
		Object[] args = ArraysUtil.join(excludeFlows, finishFlows, new Object[] {st, et}, flows, new Object[] {st, et});
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	
	
	public List<Map<String,Object>> getTransferOrderByFlowsAndTargetDc(String targetDc, String sourceDc, String st, String et, String finishSt, String finishEt, String[] flows, String[] finishFlows, String[] excludeFlows){
		StringBuilder sql = new StringBuilder("SELECT oms_ec_doc.ecDocId,oms_ec_doc.boxQty as totalBox, finish.fin*oms_ec_doc.boxQty as finishBox, sourceDcId, targetDcId "
				+"FROM oms_ec_order_flow_history "
				+"JOIN oms_ec_dc_transfer ON oms_ec_order_flow_history.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"JOIN oms_ec_doc ON oms_ec_order_flow_history.ecDocId = oms_ec_doc.ecDocId "
				+"left JOIN (SELECT max(oms_ec_order_flow_history.updateDate) as exc, ecDocId from oms_ec_order_flow_history where flow in "+getValuesSqlMarkString(excludeFlows.length)+" group by ecDocId) "
				+"exclude on oms_ec_order_flow_history.ecDocId= exclude.ecDocId "
				+"left JOIN (SELECT count(distinct oms_ec_order_flow_history.ecDocId) as fin, ecDocId from oms_ec_order_flow_history "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(finishFlows.length)+" and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate< ? group by ecDocId) "
				+"finish on oms_ec_order_flow_history.ecDocId= finish.ecDocId "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(flows.length)+" and (exclude.exc is null or exclude.exc>oms_ec_order_flow_history.updateDate) and oms_ec_order_flow_history.updateDate >= ? "
				+ "and oms_ec_order_flow_history.updateDate< ? ");
		if(StringUtils.isNotBlank(targetDc)) {
			sql.append(" and targetDcId =  '" + targetDc + "'");
		}
		if(StringUtils.isNotBlank(sourceDc)) {
			sql.append(" and sourceDcId = '" + sourceDc +"'");
		}
		sql.append(" group by oms_ec_doc.ecDocId");
		Object[] args = ArraysUtil.join(excludeFlows, finishFlows, new Object[] {finishSt, finishEt}, flows,new Object[] {st,et}) ;
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> getTransferInOrderByFlowsAndTargetDc(String targetDc, String sourceDc, String st, String et, String finishSt, String finishEt, 
			String pickUpSt, String pickUpEt, String[] flows, String[] finishFlows, String[] excludeFlows){
		StringBuilder sql = new StringBuilder("SELECT sum(oms_ec_doc.boxQty) as totalBox, sum(finish.fin*oms_ec_doc.boxQty) as finishBox, sourceDcId, targetDcId "
				+"FROM oms_ec_order_flow_history "
				+"JOIN oms_ec_dc_transfer ON oms_ec_order_flow_history.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"JOIN oms_ec_doc ON oms_ec_order_flow_history.ecDocId = oms_ec_doc.ecDocId "
				+"left JOIN (SELECT max(oms_ec_order_flow_history.updateDate) as exc, ecDocId from oms_ec_order_flow_history where flow in "+getValuesSqlMarkString(excludeFlows.length)+" group by ecDocId) "
				+"exclude on oms_ec_order_flow_history.ecDocId= exclude.ecDocId "
				+"left JOIN (SELECT count(distinct oms_ec_order_flow_history.ecDocId) as fin, ecDocId from oms_ec_order_flow_history "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(finishFlows.length)+" and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate< ? group by ecDocId) "
				+"finish on oms_ec_order_flow_history.ecDocId= finish.ecDocId "
//				+"JOIN (SELECT count(distinct oms_ec_order_flow_history.ecDocId) as fin, ecDocId from oms_ec_order_flow_history where oms_ec_order_flow_history.flow in ('"+Pa.DOC_FLOW_COLLECTED_BY_WMS_APP+"') "
//				+"and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate< ? group by ecDocId) "
//				+"arriveHub on oms_ec_order_flow_history.ecDocId= arriveHub.ecDocId "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(flows.length)+" and (exclude.exc is null "
//				+ "or exclude.exc>oms_ec_order_flow_history.updateDate"
				+ ") and oms_ec_order_flow_history.updateDate >= ? "
				+ "and oms_ec_order_flow_history.updateDate< ? ");
		if(StringUtils.isNotBlank(targetDc)) {
			sql.append(" and targetDcId =  '" + targetDc + "'");
		}
		if(StringUtils.isNotBlank(sourceDc)) {
			sql.append(" and sourceDcId = '" + sourceDc +"'");
		}
		sql.append(" group by sourceDcId,targetDcId");
//		Object[] args = ArraysUtil.join(excludeFlows, finishFlows, new Object[] {finishSt, finishEt}, new Object[] {pickUpSt, pickUpEt}, flows,new Object[] {st,et}) ;
		Object[] args = ArraysUtil.join(excludeFlows, finishFlows, new Object[] {finishSt, finishEt}, flows,new Object[] {st,et}) ;
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> getLeaveWarehouseOrderByFlowsAndTargetDc(String targetDc, String sourceDc, String date, String st, String et, String startFlow, String[] finishFlows, String[] excludeFlows){
		StringBuilder sql = new StringBuilder("select count(oms_ec_doc.ecDocId) as totalOrder, count(finish.fin) as finishOrder, sum(finish.fin * oms_ec_doc.boxQty) as finishBox,sourceDcId, targetDcId "
				+"FROM oms_ec_order_flow_history "
				+"join oms_ec_doc ON oms_ec_order_flow_history.ecDocId = oms_ec_doc.ecDocId "
				+"JOIN oms_ec_dc_transfer ON oms_ec_order_flow_history.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"left JOIN (SELECT max(oms_ec_order_flow_history.updateDate) as exc, ecDocId from oms_ec_order_flow_history where flow in "+getValuesSqlMarkString(excludeFlows.length)+" group by ecDocId) "
				+"exclude on oms_ec_order_flow_history.ecDocId= exclude.ecDocId "
				+"left JOIN (SELECT count(distinct oms_ec_order_flow_history.ecDocId) as fin, ecDocId from oms_ec_order_flow_history "
				+"where oms_ec_order_flow_history.flow in "+getValuesSqlMarkString(finishFlows.length)+" and oms_ec_order_flow_history.updateDate >= ? "
				+"and oms_ec_order_flow_history.updateDate<= ? group by ecDocId) finish "
				+"on oms_ec_order_flow_history.ecDocId= finish.ecDocId "
				+"where oms_ec_order_flow_history.flow in ('"+startFlow+"')  and oms_ec_doc.bookingCollectDate = ? and "
				+"(exclude.exc is null or exclude.exc>oms_ec_order_flow_history.updateDate) and oms_ec_doc.shipMode = "+Pa.EC_DOC_SHIP_MODE_B+" ");

		if(StringUtils.isNotBlank(targetDc)) {
			sql.append(" and targetDcId =  '" + targetDc + "'");
		}
		if(StringUtils.isNotBlank(sourceDc)) {
			sql.append(" and sourceDcId = '" + sourceDc +"'");
		}
		sql.append(" group by sourceDcId,targetDcId");
		Object[] args = ArraysUtil.join(excludeFlows, finishFlows, new Object[] {st, et, date} ) ;
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> getDeliveryOrderByFlowsAndTargetDc(String targetDc, String shipDate){
		String[] excludeFlows = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		StringBuilder sql = new StringBuilder("SELECT DISTINCT(oms_ec_doc.ecDocId) , oms_ec_doc.boxQty "
				+"FROM oms_town_router_detail_doc "
				+"JOIN oms_town_router_doc ON oms_town_router_detail_doc.townRouterDocId = oms_town_router_doc.townRouterDocId "
				+"JOIN oms_ec_doc ON oms_ec_doc.ecDocId = oms_town_router_detail_doc.ecDocId "
				+"JOIN oms_ec_dc_transfer ON oms_town_router_detail_doc.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"left JOIN (SELECT max(oms_ec_order_flow_history.updateDate) as exc, ecDocId from oms_ec_order_flow_history where flow in "+getValuesSqlMarkString(excludeFlows.length)+" group by ecDocId) "
				+"exclude on oms_town_router_detail_doc.ecDocId= exclude.ecDocId "
				+"WHERE shipDate= ? AND oms_town_router_detail_doc.townRouterDocId like 'T%' "
				+"AND (exclude.exc is null or exclude.exc>oms_town_router_doc.updateDate) "
				+"AND oms_ec_dc_transfer.targetDcId= ? ");
		Object[] args = ArraysUtil.join(excludeFlows,new Object[] {shipDate,targetDc});
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> getReturnOrder(String targetDc, String shipDate, String st, String et, String[] startFlows, String[] excludeFlows){
		StringBuilder sql = new StringBuilder("SELECT DISTINCT(oms_ec_doc.ecDocId) , oms_ec_doc.boxQty FROM oms_town_router_detail_doc "
				+"JOIN oms_town_router_doc ON oms_town_router_detail_doc.townRouterDocId = oms_town_router_doc.townRouterDocId "
				+"JOIN oms_ec_dc_transfer ON oms_town_router_detail_doc.ecDocId = oms_ec_dc_transfer.ecDocId "
				+"JOIN oms_ec_doc ON oms_ec_doc.ecDocId = oms_town_router_detail_doc.ecDocId "
				+"JOIN (SELECT count(oms_ec_order_flow_history.ecDocId) as exc, ecDocId, max(updateDate) AS lastUpdate from oms_ec_order_flow_history "
				+"where flow in "+getValuesSqlMarkString(startFlows.length)+" and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate<= ? group by ecDocId) include "
				+"on oms_town_router_detail_doc.ecDocId= include.ecDocId "
				+"left JOIN (SELECT count(oms_ec_order_flow_history.ecDocId) as exc, ecDocId, max(updateDate) AS lastUpdate from oms_ec_order_flow_history "
				+"where flow in "+getValuesSqlMarkString(excludeFlows.length)+" and oms_ec_order_flow_history.updateDate >= ? and oms_ec_order_flow_history.updateDate<= ? group by ecDocId) exclude "
				+"on oms_town_router_detail_doc.ecDocId= exclude.ecDocId "
				+"WHERE shipDate= ? and (exclude.exc is null OR exclude.lastUpdate <= include.lastUpdate ) AND oms_town_router_detail_doc.townRouterDocId like 'T%' AND oms_ec_dc_transfer.targetDcId= ? ");
		
		Object[] args = ArraysUtil.join(startFlows, new Object[] {st,et}, excludeFlows, new Object[] {st,et,shipDate,targetDc});
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> getFinishDeliveryOrderByFlowsAndTargetDc(String st, String et,Object[] ids, Object[] finishFlows){
		StringBuilder sql = new StringBuilder("SELECT h2.flow as flowEnd, SUM(boxQty) as totalBox FROM oms_ec_doc "
				+"JOIN (SELECT ecDocId, MAX(historyId) AS maxHistoryId FROM oms_ec_order_flow_history "
				+"WHERE ecDocId IN "+getValuesSqlMarkString(ids.length)+" "
				+"AND flow IN "+getValuesSqlMarkString(finishFlows.length)+" and updateDate>= ? and updateDate<= ? "
				+"GROUP BY ecDocId) h1 ON h1.ecDocId = oms_ec_doc.ecDocId "
				+"JOIN (SELECT DISTINCT ecDocId, historyId, flow "
				+"FROM oms_ec_order_flow_history "
				+"WHERE ecDocId IN "+getValuesSqlMarkString(ids.length)+" and updateDate>= ? and updateDate<= ? "
				+"GROUP BY ecDocId, historyId,flow) h2 ON h1.ecDocId = h2.ecDocId AND h1.maxHistoryId = h2.historyId "
				+"GROUP BY h2.flow" );
		Object[] args = ArraysUtil.join(ids,finishFlows,new Object[] {st,et}, ids, new Object[] {st,et}) ;
		SystemOutPrintHandler.printSql(sql.toString(), args);
		List<Map<String, Object>> rs = jdbcTemplate.queryForList(sql.toString(),args);
		return rs;
	}
	
	public List<Map<String,Object>> findDisName(){
		StringBuilder sql = initSql("select * from "+Dc.TABLE_NAME
				+" where "+Dc.FIELD_DC_ID+" !=? order by "+Dc.FIELD_SHOW_ORDER);
		Object[] params = init("DC");
		SystemOutPrintHandler.printSql(sql,params);
		List<Map<String,Object>> rs = jdbcTemplate.queryForList(sql.toString(),params);
		return rs;
	}
	
	public String findDisName(String dcId){
		StringBuilder sql = initSql("select * from "+Dc.TABLE_NAME
				+" where "+Dc.FIELD_DC_ID+" = ? order by "+Dc.FIELD_SHOW_ORDER);
		Object[] params = new Object[] {dcId};
		SystemOutPrintHandler.printSql(sql,params);
		List<Map<String,Object>> rs = jdbcTemplate.queryForList(sql.toString(),params);
		if(!rs.isEmpty()) {
			return rs.get(0).get(Dc.FIELD_SHOW_DIS_NAME).toString();
		}
		return "";
	}
}
