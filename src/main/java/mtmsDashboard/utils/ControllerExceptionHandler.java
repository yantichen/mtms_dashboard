package mtmsDashboard.utils;

import org.apache.logging.log4j.Logger;

import mtmsDashboard.common.Pa;


public class ControllerExceptionHandler {
	
	public static void printAndLogException(Logger log,String methodName,Exception e){
		if(Pa.BUG_SYS_TRACK_PRINT){
			e.printStackTrace();
		}
		log.fatal(methodName, e);
	}
}
