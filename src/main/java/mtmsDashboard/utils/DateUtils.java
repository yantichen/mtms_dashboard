package mtmsDashboard.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;


public class DateUtils {
	
	final public static String DATE_TIME_FORMAT_14 = "yyyyMMddHHmmss";
	final public static String DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
	final public static String DATE_FORMAT_8 = "yyyyMMdd";
	final public static String DATE_FORMAT = "yyyy/MM/dd";
	final public static String TIME_FORMAT = "HH:mm:ss";
	
	public static String UnixTimestampToDate14(String d) {
		long tsmplg = new Double(Double.parseDouble(d)).longValue() * 1000;

		Date date = new Date(tsmplg);

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT_14);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(date);
	}
	
	public static String formatDate14(String date){
		if(date!=null&&date.length()==14){
			return String.format("%s/%s/%s %s:%s:%s", 
					date.substring(0, 4),
					date.substring(4, 6),
					date.substring(6, 8),
					date.substring(8, 10),
					date.substring(10, 12),
					date.substring(12)
					);
					
		}
		return date;
	}
	public static String formatDate8(String date){
		if(date!=null&&date.length()>=8){
			return String.format("%s/%s/%s", 
					date.substring(0, 4),
					date.substring(4, 6),
					date.substring(6, 8)
					);
			
		}
		return date;
	}
	
	// 一天的結束
	public static String dateToEndDateTime(String date){
		return date.substring(0, 8)+"235959";
	}
	
	// 一天的開始
	public static String dateToStartDateTime(String date){
		return date.substring(0, 8)+"000000";
	}
	
	// 取得當月最後一天
	public static String monthToEndDateTime(String date){
		
		// 取得當月第一天
		date = monthToStartDateTime(date);
		
		LocalDateTime localDateTime = parseStringToDate(date, DATE_TIME_FORMAT_14);
		localDateTime.plusMonths(1);	//加一個月
		localDateTime.plusDays(-1);	//減一天就是當月最後日
		
		return toString(localDateTime, DATE_FORMAT_8)+"235959";
	}
	
	// 取得當月第一天
	public static String monthToStartDateTime(String date){
		
		return date.substring(0, 6)+"01000000";
	}
	
	public static boolean isAfter(String dt14Str1, String dt14Str2) {		
		
		LocalDateTime dt1 = parseStringToDate(dt14Str1, DATE_TIME_FORMAT_14);
		LocalDateTime dt2 = parseStringToDate(dt14Str2, DATE_TIME_FORMAT_14);
		return dt1.isAfter(dt2);
	}
	
	public static boolean isBefore(String dt14Str1, String dt14Str2) {		
		
		LocalDateTime dt1 = parseStringToDate(dt14Str1, DATE_TIME_FORMAT_14);
		LocalDateTime dt2 = parseStringToDate(dt14Str2, DATE_TIME_FORMAT_14);
		
		return dt1.isBefore(dt2);
	}
	
	public static boolean isEqual(String dt14Str1, String dt14Str2) {		
		
		LocalDateTime dt1 = parseStringToDate(dt14Str1, DATE_TIME_FORMAT_14);
		LocalDateTime dt2 = parseStringToDate(dt14Str2, DATE_TIME_FORMAT_14);
		
		return dt1.isEqual(dt2);
	}
	
	// 回傳dt14Str是否在dt14FromStr和dt14ToStr之間
	public static boolean isInDuration(String dt14Str, String dt14FromStr, String dt14ToStr) {		
		
		if(dt14Str.length()==14 && dt14FromStr.length()==14 && dt14ToStr.length()==14) {
			LocalDateTime dt = parseStringToDate(dt14Str, DATE_TIME_FORMAT_14);
			LocalDateTime dtFrom = parseStringToDate(dt14FromStr, DATE_TIME_FORMAT_14);
			LocalDateTime dtTo = parseStringToDate(dt14ToStr, DATE_TIME_FORMAT_14);
			return !dt.isBefore(dtFrom) && !dt.isAfter(dtTo);
		}else {
			LocalDateTime dt = parseStringToDate(dt14Str.substring(0,8), DATE_FORMAT_8);
			LocalDateTime dtFrom = parseStringToDate(dt14FromStr.substring(0,8), DATE_FORMAT_8);
			LocalDateTime dtTo = parseStringToDate(dt14ToStr.substring(0,8), DATE_FORMAT_8);
			
			return !dt.isBefore(dtFrom) && !dt.isAfter(dtTo);
		}
		
	}
	
	public static boolean isAfterBookingCoolectShipDateTime(String uploadDate14Str,int ecCompanyCloseHour){
		if(uploadDate14Str.length()!=14){
			uploadDate14Str=getNowTimeString();
		}
		
		LocalDateTime ecCompanyCloseHourJd = parseStringToDate(uploadDate14Str.substring(0,8)+String.format("%02d", ecCompanyCloseHour)+"0000", "yyyyMMddHHmmss");//中午
		
		LocalDateTime uploadDateJD = parseStringToDate(uploadDate14Str, DATE_TIME_FORMAT_14);
		
		return uploadDateJD.isAfter(ecCompanyCloseHourJd);
	}
	
	public static long getDiffDay(String dStr1, String dStr2) {
		LocalDateTime d1 = parseStringToDate(dStr1, DATE_FORMAT_8);
		LocalDateTime d2 = parseStringToDate(dStr2, DATE_FORMAT_8);
		
		Duration dur = Duration.between(d1, d2);
		return dur.toDays();
	}
	
	public static String getNowTimeString() {
		return getNowTimeString(false);
	}
	
	public static String getTodayString() {
		return getTodayString(false);
	}
	
	public static String getNowTimeString(boolean format) {
		if(format) {
			return DateTimeFormatter.ofPattern(DATE_TIME_FORMAT).format(LocalDateTime.now());
		}else {
			return DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_14).format(LocalDateTime.now());
		}
		
	}
	
	public static String getTodayString(boolean format) {
		if(format) {
			return DateTimeFormatter.ofPattern(DATE_FORMAT).format(LocalDateTime.now());
		}else {
			return DateTimeFormatter.ofPattern(DATE_FORMAT_8).format(LocalDateTime.now());
		}
	}
	
	
	public static String toString(LocalDateTime dateTime, String fomat) {
		return DateTimeFormatter.ofPattern(fomat).format(dateTime);
	}
	
	public static LocalDateTime parseStringToDate(String dateTimeStr, String format) {
		
		if(!format.toUpperCase().contains("H")) {
			return LocalDate.parse(dateTimeStr, DateTimeFormatter.ofPattern(format)).atStartOfDay();
		}
		
		return LocalDateTime.parse(dateTimeStr, DateTimeFormatter.ofPattern(format));
	}
	
	public static String parseStringToTime(String dateTimeStr, String format) {
		return LocalTime.parse(dateTimeStr.substring(8), DateTimeFormatter.ofPattern(format)).toString();
	}
	
	
	// 取得月統計區間
    public static Date[] getMonthDuration(Date chkDate) throws ParseException {
//    	LocalDate date = LocalDate.of(2020, 01, 1);
    	LocalDate date = dateToLocalDate(chkDate);
    	
		LocalDate begin = null, end = null;
		if(date.getDayOfMonth() == 1){
			date = date.minusMonths(1);
		}
		begin = date.withDayOfMonth(1);
		end = date.withDayOfMonth(date.lengthOfMonth());
		//System.out.println(begin + " " + end);
		Date[] duration = toDateDuration(begin,end);
		//System.out.println(duration[0] + " " + duration[1]);
		return duration;
    }
	
	 // 取得季統計區間
    public static Date[] getSeasonDuration(Date chkDate) throws ParseException {
//    	LocalDate date = LocalDate.of(2020, 11, 25);
    	LocalDate date = dateToLocalDate(chkDate);
    	LocalDate begin = null, end = null;
    	int monthOfYear = date.getDayOfMonth() == 1 ? date.getMonthValue() - 1 : date.getMonthValue();
    	
    	if(monthOfYear <= 3){
    		begin = LocalDate.of(date.getYear(), 01, 1);
    	}else if(monthOfYear <= 6){
    		begin = LocalDate.of(date.getYear(), 04, 1);
    	}else if(monthOfYear <= 9){
    		begin = LocalDate.of(date.getYear(), 07, 1);
    	}else if(monthOfYear <= 12){	
    		begin = LocalDate.of(date.getYear(), 10, 1);
    	}
		end = date;
		//System.out.println(begin + " " + end);
		Date[] duration = toDateDuration(begin,end);
		//System.out.println(duration[0] + " " + duration[1]);
		return duration;
    }
	
	public static Date[] getYearDuration(Date chkDate) throws ParseException {
//    	LocalDate date = LocalDate.of(2020, 11, 25);
    	LocalDate date = dateToLocalDate(chkDate);
    	LocalDate begin = null, end = null;
    	begin = LocalDate.of(date.getYear(),01,01);
		end = date;
		//System.out.println(begin + " " + end);
		Date[] duration = toDateDuration(begin,end);
		//System.out.println(duration[0] + " " + duration[1]);
		return duration;
    }
	
	public static Date[] toDateDuration(LocalDate begin,LocalDate end){
    	Date[] duration = new Date[2];
		duration[0] = getBeginDate(localDateToDate(begin));
		duration[1] = getEndDate(localDateToDate(end));
		return duration;
    }
    
    public static Date localDateToDate(LocalDate localDate){
    	return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
    
    public static LocalDate dateToLocalDate(Date date){
    	return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
    /**
	 * 取得傳入日期加入時分秒的格式
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndDate(Date date) {
		if (null == date)
			return null;
		Calendar transDate = Calendar.getInstance();
		transDate.setTime(date);
		transDate.set(Calendar.HOUR_OF_DAY, 23);
		transDate.set(Calendar.MINUTE, 59);
		transDate.set(Calendar.SECOND, 59);
		transDate.set(Calendar.MILLISECOND, 0);
		date = transDate.getTime();
		return date;
	}

	/**
	 * 取得傳入日期加入時分秒的格式
	 * 
	 * @param date
	 * @return
	 */
	public static Date getBeginDate(Date date) {
		if (null == date)
			return null;
		Calendar transDate = Calendar.getInstance();
		transDate.setTime(date);
		transDate.set(Calendar.HOUR_OF_DAY, 0);
		transDate.set(Calendar.MINUTE, 0);
		transDate.set(Calendar.SECOND, 0);
		date = transDate.getTime();
		return date;
	}
	
	/**
	 * 將日期轉換成指定之格式
	 * 
	 * @param date
	 * @param pattern
	 * @return 轉換後之日期格式
	 * @see java.text.SimpleDateFormat
	 */
	public final static String getDateString(Date date, String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		if (null == date) {
			return null;
		}
		return dateFormat.format(date);
	}

	/**
	 * pattern yyyyMMddHHmmss<br>
	 */
	public static final String pattern14 = "yyyyMMddHHmmss";
	
	/**
	 * pattern yyyyMMdd<br>
	 * java.sql.Date的格式
	 */
	public static final String pattern9 = "yyyyMMdd";
	
	/**
	 * 將日期轉換成指定之格式
	 * 
	 * @param date
	 * @param pattern
	 * @return 轉換後之日期格式
	 * @throws ParseException - if the beginning of the specified string cannot be
	 *                        parsed.
	 * @see java.text.SimpleDateFormat
	 */
	public final static Date toDate(String date, String pattern) throws ParseException {
		if (StringUtils.isBlank(date))
			return null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		Date a = dateFormat.parse(date);
		return a;
	}

	
}
