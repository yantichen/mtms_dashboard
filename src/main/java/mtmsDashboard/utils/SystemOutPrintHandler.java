package mtmsDashboard.utils;

import java.util.Collection;


/******************************************
 * 顯示sql語法
 *
 * @author tim create by 2017/07/16
 *******************************************/
public class SystemOutPrintHandler {

	private static StringBuilder createStringBuilder(StringBuilder sql) {
		return new StringBuilder(sql.toString());
	}
	
//	public static void printSql(String sql, Collection<E> conditions) {
//		printSql(new StringBuilder(sql), conditions);
//	}
	
	public static void printSql(String sql, Object... conditions) {
		printSql(new StringBuilder(sql), conditions);
	}
	
	public static void printSql(String sql) {
		printSql(new StringBuilder(sql));
	}

	public static void printSql(StringBuilder sql, Object[] params) {
		try {
			sql = createStringBuilder(sql);
			if (params != null) {
				for (Object s : params) {
					int idx = sql.indexOf("?");
					if (s != null) {
						if(idx<0) {
							sql.append(" :"+String.valueOf(s));
						}else {
							sql.replace(idx, idx + 1, s instanceof String ? "'"+s+"'" : String.valueOf(s));
						}
						
					}else {
						if(idx<0) {
							sql.append(" :NULL");
						}else {
							sql.replace(idx, idx + 1, "NULL");
						}
					}
				}
			}
			printSql(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public static <E> void printSql(StringBuilder sql, Collection<E> params) {
		try {
			sql = createStringBuilder(sql);
			if (params != null) {
				for (Object s : params) {
					if (s != null) {
						int idx = sql.indexOf("?");
						if(idx<0) {
							sql.append(" :"+String.valueOf(s));
						}else {
							sql.replace(idx, idx + 1, s==null? "NULL" : "'"+String.valueOf(s)+"'");
						}
					}
						
				}
			}
			printSql(sql);
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static <E> void printSql(String sql, Collection<E> params) {
		printSql(new StringBuilder(sql), params);
	}

	public static void printSql(StringBuilder sql, String... ss) {
		try {
			sql = createStringBuilder(sql);
			if (ss != null) {
				for (String s : ss) {
					int idx = sql.indexOf("?");
					if(s==null) {
						if(idx<0) {
							sql.append(" :NULL");
						}else {
							sql.replace(idx, idx + 1, "NULL");
						}
					}else {
						if(idx<0) {
							sql.append(" :"+String.valueOf(s));
						}else {
							sql.replace(idx, idx + 1, "'"+String.valueOf(s)+"'");
						}
					}
					
					
				}
			}
			printSql(sql);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private static void printSql(StringBuilder sql) {
		
		System.out.println( DateUtils.getNowTimeString(true)+"\t"+ sql.toString());
	}
	
	public static void printLog(String log) {
		
		System.out.println( DateUtils.getNowTimeString(true)+"\t"+ log);
		
	}
	
	public static void printErrLog(String log) {		
		
		System.err.println( DateUtils.getNowTimeString(true)+"\t"+ log);
		
	}
}
