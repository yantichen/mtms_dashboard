package mtmsDashboard.common;

import java.util.Arrays;

public class CommonDao {
	
	protected static Object[] init(Object... objects) {
		return objects;
	}
	
	protected static StringBuilder initSql(String sqlstr) {
		StringBuilder sql=new StringBuilder(sqlstr);
		return sql;
	}
	
	protected static String getValuesSqlMarkString(int valueNum) {
		String[] ss = new String[valueNum];
		Arrays.fill(ss, "?");
		return "("+String.join(",", ss)+")";
	}
	
	protected static String getColumnsString(String... ss) {		
		return "("+String.join(",", ss)+")";
	}
	
	protected static String getColumnsString(Iterable<? extends CharSequence> ss) {		
		return "("+String.join(",", ss)+")";
	}
	
	protected static String getValuesString(String... ss) {		
		return "('"+String.join("','", ss)+"')";
	}
	
	protected static String getValuesString(Iterable<? extends CharSequence> ss) {		
		return "('"+String.join("','", ss)+"')";
	}

}
