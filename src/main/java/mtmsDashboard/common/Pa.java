package mtmsDashboard.common;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;


public class Pa {
	
	//directory name
	public static String MTMS_DIR=System.getProperty("user.home")+File.separator+"mtms"+File.separator+"dashboard";
	
	//db setting
	final public static String DB;
	final public static String DB_NAME;
	final public static String DB_USER;
	final public static String DB_PWD;
		
	static {
		
		File config = new File(MTMS_DIR+File.separator+"config.txt");
		System.out.println(MTMS_DIR+File.separator+"config.txt");
		Map<String,String> configMap = new HashMap<String,String>();
		if(!config.exists()) {
			System.out.println("Config file not found. Please put config.txt file in this directory : "+System.getProperty("user.home")+File.separator);
		}
		config = new File(MTMS_DIR+File.separator+"config.txt");
		String configStr = "";
		try {
			configStr = FileUtils.readFileToString(config);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Incorrect config file format");
		}
		Gson gson = new Gson();
		configMap = gson.fromJson(configStr, Map.class);
		DB=configMap.get("DB_ADDRESS");
		DB_NAME=configMap.get("DB_NAME");
		DB_USER=configMap.get("DB_USER");
		DB_PWD=configMap.get("DB_PWD");
		CDN_IP=configMap.get("CDN_IP");
		JS_VERSION=configMap.get("JS_VERSION");
		LOCAL_IP=configMap.get("LOCAL_IP");
		TEST_MODE=configMap.get("TEST_MODE");
		BUG_SYS_TRACK_PRINT=configMap.containsKey("BUG_SYS_TRACK_PRINT") && configMap.get("BUG_SYS_TRACK_PRINT").toString().equals("Y") ? true : false;
		PRINT_SQL=configMap.containsKey("PRINT_SQL") && configMap.get("PRINT_SQL").toString().equals("Y") ? true : false;
	}
	
	//mode
	final public static String TEST_MODE;//Y:測試機 其他:正式機
	public final static int EC_DOC_SHIP_MODE_A=1; //集轉配
	public final static int EC_DOC_SHIP_MODE_B=2; //在倉發貨
	public final static int EC_DOC_SHIP_MODE_C=3; //集貨入庫
	public final static int EC_DOC_SHIP_MODE_D=4; //退貨單
	public final static int EC_DOC_SHIP_MODE_E=5; //轉運單
	public final static String ACCOUNT_ROLE_APPWMS="APPWMS";
	
	final public static String LOCAL_IP;
	final public static String CDN_IP;
	final public static String CDN=CDN_IP+"mtms_cdn/";
	final public static String CDN_LIB_LINK=CDN_IP+"mtms_cdn";
	final public static String CDN_LIB_LINK_V2=CDN_IP+"mtms_cdnv2";
	final public static String JS_VERSION;
	final public static boolean BUG_SYS_TRACK_PRINT;
	final public static boolean PRINT_SQL;
	
	final public static String RS_MSG_TYPE_SUCCESS="success";
	final public static String RS_MSG_TYPE_ERROR="error";
	final public static String RS_MSG_TYPE_DCERROR="dcError";
	final public static String RS_MSG_TYPE_DIFFERENTGROUPS="differentGroups";
	final public static String RS_MSG_TYPE_REPEAT="repeat";
	final public static String RS_MSG_TYPE_FINISH="finish";
	public static RsMsg error=new RsMsg(RS_MSG_TYPE_ERROR, "error");
	public static RsMsg success=new RsMsg(RS_MSG_TYPE_SUCCESS, "success");
	
	//flow
	public final static String DOC_FLOW_INIT="0";//已上傳
	public final static String DOC_FLOW_EMAIL="200";//已通知集貨(Email 12:00已經發送)
	public final static String DOC_FLOW_NOTIFY_OUTBOUNT="100";//理貨通知(在倉發貨使用，對在倉發貨EC來說也是結單的意思)
	public final static String DOC_FLOW_START_OUTBOUNT="110";//理貨通知(在倉發貨使用，越庫作業)
	public final static String DOC_FLOW_CLOSE_CREATE="220";//已結單(手動結單)、或是CWMS出庫完成
	public final static String DOC_FLOW_COLLECTED="225";//qrcode集貨完成
	public final static String DOC_FLOW_AI_CREATE_DATA="230";//排車中(拋轉至AI server) 等待討論 不該列入作業節點
	public final static String DOC_FLOW_COLLECTED_BY_WMS_APP="235";//倉管app集貨回倉
	public final static String DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP="238";//裝載攏車貼單
	public final static String DOC_FLOW_TRANSFER_DC_ING="240";//QR code轉運中
	public final static String DOC_FLOW_AI_COUNT="250";//AI排車完成，等待GGV拋轉或自車配送
	public final static String DOC_FLOW_ORA="300";//已拋轉
	public final static String DOC_FLOW_ARRIVE_END_DC="400";//抵達配送dc
	public final static String DOC_FLOW_PIC="450";//準備出貨(GGV)
	public final static String DOC_FLOW_LOADED="600";//配貨裝車
	public final static String DOC_FLOW_ARRIVE_HUB="700";//分貨抵達
	public final static String DOC_FLOW_TRA="800";//配送中
	public final static String DOC_FLOW_DRIVER_TRANSFER_BOX="650";//貨品移轉
	public final static String DOC_FLOW_POD="900";//貨品送達 (GGV and Roundday)
	public final static String DOC_FLOW_EXC="1000";//訂單異常(收件人不在家)
	public final static String DOC_FLOW_BATCH_EXC="1010";//訂單異常(分貨站整批退回)
	public final static String DOC_FLOW_WMS_GET_GOOD_BOX="1100";//WMS驗收異常(包裹完好)
	public final static String DOC_FLOW_WMS_GET_BAD_BOX="1150";//WMS訂單異常(包裹異常)
	public final static String DOC_FLOW_NO_DELIVERY="1200";//訂單取消不配
	public final static String DOC_FLOW_SHIP_RECEIPT="1350";//回單完成
	

}
