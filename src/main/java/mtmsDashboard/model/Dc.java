package mtmsDashboard.model;

import java.util.HashMap;
import java.util.Map;

public class Dc {
	public final static String TABLE_NAME = "dc";
	public final static String FIELD_DC_ID = "dcId";
	public final static String FIELD_DIS_NAME = "disName";
	public final static String FIELD_ZIP_CODE = "zipCode";
	public final static String FIELD_ADDR = "addr";
	public final static String FIELD_PHONE = "phone";
	public final static String FIELD_CONTACT = "contact";
	public final static String FIELD_CONTACT_PHONE = "contactPhone";
	public final static String FIELD_CONTACT_EMAIL = "contactEmail";
	public final static String FIELD_LAT = "lat";
	public final static String FIELD_LNG = "lng";
	public final static String FIELD_SHIP_TYPE = "shipType";
	public final static String FIELD_UPDATE_DATE = "updateDate";
	public final static String FIELD_UPDATOR = "updator";
	public final static String FIELD_DISTRIBUTION_NAME = "distributionName";
	public final static String FIELD_REGION_ID = "regionId";
	public final static String FIELD_COMPANY_ID = "companyId";
	public final static String FIELD_TABLE_ID = "tableId";
	public final static String FIELD_ENABLE = "enable";
	public final static String FIELD_SHOW_ORDER = "showOrder";
	public final static String FIELD_SHOW_DIS_NAME = "showDisName";

	public final static String[] FIELD_NAME_ARRAY = {FIELD_DC_ID, FIELD_DIS_NAME, FIELD_ZIP_CODE, FIELD_ADDR, FIELD_PHONE, FIELD_CONTACT, FIELD_CONTACT_PHONE, FIELD_CONTACT_EMAIL, FIELD_LAT, FIELD_LNG, FIELD_SHIP_TYPE, FIELD_UPDATE_DATE, FIELD_UPDATOR, FIELD_DISTRIBUTION_NAME, FIELD_REGION_ID, FIELD_COMPANY_ID, FIELD_TABLE_ID, FIELD_ENABLE, FIELD_SHOW_ORDER, FIELD_SHOW_DIS_NAME};

	
	private String dcId;
	private String disName;
	private String zipCode;
	private String addr;
	private String phone;
	private String contact;
	private String contactPhone;
	private String contactEmail;
	private String lat;
	private String lng;
	private String shipType;
	private String updateDate;
	private String updator;
	private String distributionName;
	private String regionId;
	private String companyId;
	private String tableId;
	private String enable;
	private int showOrder;
	private String showDisName;

	public Dc(Map<String, Object> map) {
		super();
		this.dcId = String.valueOf(map.get(FIELD_DC_ID));
		this.disName = String.valueOf(map.get(FIELD_DIS_NAME));
		this.zipCode = String.valueOf(map.get(FIELD_ZIP_CODE));
		this.addr = String.valueOf(map.get(FIELD_ADDR));
		this.phone = String.valueOf(map.get(FIELD_PHONE));
		this.contact = String.valueOf(map.get(FIELD_CONTACT));
		this.contactPhone = String.valueOf(map.get(FIELD_CONTACT_PHONE));
		this.contactEmail = String.valueOf(map.get(FIELD_CONTACT_EMAIL));
		this.lat = String.valueOf(map.get(FIELD_LAT));
		this.lng = String.valueOf(map.get(FIELD_LNG));
		this.shipType = String.valueOf(map.get(FIELD_SHIP_TYPE));
		this.updateDate = String.valueOf(map.get(FIELD_UPDATE_DATE));
		this.updator = String.valueOf(map.get(FIELD_UPDATOR));
		this.distributionName = String.valueOf(map.get(FIELD_DISTRIBUTION_NAME));
		this.regionId = String.valueOf(map.get(FIELD_REGION_ID));
		this.companyId = String.valueOf(map.get(FIELD_COMPANY_ID));
		this.tableId = String.valueOf(map.get(FIELD_TABLE_ID));
		this.enable = String.valueOf(map.get(FIELD_ENABLE));
		try {
			this.showOrder = Integer.parseInt(String.valueOf(map.get(FIELD_SHOW_ORDER)));
		} catch (Exception e) {
			this.showOrder = 0;
		}		
		this.showDisName = String.valueOf(map.get(FIELD_SHOW_DIS_NAME));
	}

	public String getDcId() {
		return dcId;
	}

	public void setDcId(String dcId) {
		this.dcId = dcId;
	}

	public String getDisName() {
		return disName;
	}

	public void setDisName(String disName) {
		this.disName = disName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getShipType() {
		return shipType;
	}

	public void setShipType(String shipType) {
		this.shipType = shipType;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdator() {
		return updator;
	}

	public void setUpdator(String updator) {
		this.updator = updator;
	}

	public String getDistributionName() {
		return distributionName;
	}

	public void setDistributionName(String distributionName) {
		this.distributionName = distributionName;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	public int getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(int showOrder) {
		this.showOrder = showOrder;
	}

	public String getShowDisName() {
		return showDisName;
	}

	public void setShowDisName(String showDisName) {
		this.showDisName = showDisName;
	}

	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<>();
		map.put(FIELD_DC_ID, dcId);
		map.put(FIELD_DIS_NAME, disName);
		map.put(FIELD_ZIP_CODE, zipCode);
		map.put(FIELD_ADDR, addr);
		map.put(FIELD_PHONE, phone);
		map.put(FIELD_CONTACT, contact);
		map.put(FIELD_CONTACT_PHONE, contactPhone);
		map.put(FIELD_CONTACT_EMAIL, contactEmail);
		map.put(FIELD_LAT, lat);
		map.put(FIELD_LNG, lng);
		map.put(FIELD_SHIP_TYPE, shipType);
		map.put(FIELD_UPDATE_DATE, updateDate);
		map.put(FIELD_UPDATOR, updator);
		map.put(FIELD_DISTRIBUTION_NAME, distributionName);
		map.put(FIELD_REGION_ID, regionId);
		map.put(FIELD_COMPANY_ID, companyId);
		map.put(FIELD_TABLE_ID, tableId);
		map.put(FIELD_ENABLE, enable);
		map.put(FIELD_SHOW_ORDER, showOrder);
		map.put(FIELD_SHOW_DIS_NAME, showDisName);
		return map;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addr == null) ? 0 : addr.hashCode());
		result = prime * result + ((companyId == null) ? 0 : companyId.hashCode());
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((contactEmail == null) ? 0 : contactEmail.hashCode());
		result = prime * result + ((contactPhone == null) ? 0 : contactPhone.hashCode());
		result = prime * result + ((dcId == null) ? 0 : dcId.hashCode());
		result = prime * result + ((disName == null) ? 0 : disName.hashCode());
		result = prime * result + ((distributionName == null) ? 0 : distributionName.hashCode());
		result = prime * result + ((enable == null) ? 0 : enable.hashCode());
		result = prime * result + ((lat == null) ? 0 : lat.hashCode());
		result = prime * result + ((lng == null) ? 0 : lng.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((regionId == null) ? 0 : regionId.hashCode());
		result = prime * result + ((shipType == null) ? 0 : shipType.hashCode());
		result = prime * result + ((showDisName == null) ? 0 : showDisName.hashCode());
		result = prime * result + showOrder;
		result = prime * result + ((tableId == null) ? 0 : tableId.hashCode());
		result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
		result = prime * result + ((updator == null) ? 0 : updator.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dc other = (Dc) obj;
		if (addr == null) {
			if (other.addr != null)
				return false;
		} else if (!addr.equals(other.addr))
			return false;
		if (companyId == null) {
			if (other.companyId != null)
				return false;
		} else if (!companyId.equals(other.companyId))
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (contactEmail == null) {
			if (other.contactEmail != null)
				return false;
		} else if (!contactEmail.equals(other.contactEmail))
			return false;
		if (contactPhone == null) {
			if (other.contactPhone != null)
				return false;
		} else if (!contactPhone.equals(other.contactPhone))
			return false;
		if (dcId == null) {
			if (other.dcId != null)
				return false;
		} else if (!dcId.equals(other.dcId))
			return false;
		if (disName == null) {
			if (other.disName != null)
				return false;
		} else if (!disName.equals(other.disName))
			return false;
		if (distributionName == null) {
			if (other.distributionName != null)
				return false;
		} else if (!distributionName.equals(other.distributionName))
			return false;
		if (enable == null) {
			if (other.enable != null)
				return false;
		} else if (!enable.equals(other.enable))
			return false;
		if (lat == null) {
			if (other.lat != null)
				return false;
		} else if (!lat.equals(other.lat))
			return false;
		if (lng == null) {
			if (other.lng != null)
				return false;
		} else if (!lng.equals(other.lng))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (regionId == null) {
			if (other.regionId != null)
				return false;
		} else if (!regionId.equals(other.regionId))
			return false;
		if (shipType == null) {
			if (other.shipType != null)
				return false;
		} else if (!shipType.equals(other.shipType))
			return false;
		if (showDisName == null) {
			if (other.showDisName != null)
				return false;
		} else if (!showDisName.equals(other.showDisName))
			return false;
		if (showOrder != other.showOrder)
			return false;
		if (tableId == null) {
			if (other.tableId != null)
				return false;
		} else if (!tableId.equals(other.tableId))
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		if (updator == null) {
			if (other.updator != null)
				return false;
		} else if (!updator.equals(other.updator))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Dc [dcId=" + dcId + ", disName=" + disName + ", zipCode=" + zipCode + ", addr=" + addr + ", phone="
				+ phone + ", contact=" + contact + ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail
				+ ", lat=" + lat + ", lng=" + lng + ", shipType=" + shipType + ", updateDate=" + updateDate
				+ ", updator=" + updator + ", distributionName=" + distributionName + ", regionId=" + regionId
				+ ", companyId=" + companyId + ", tableId=" + tableId + ", enable=" + enable + ", showOrder="
				+ showOrder + ", showDisName=" + showDisName + "]";
	}
}
