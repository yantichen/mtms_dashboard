package mtmsDashboard.model;

public class Dashboard {
	public final static String FIELD_FINISH_COLLECT_PACKAGES = "finishCollect";//已回倉
	public final static String FIELD_COLLECT_PACKAGES = "totalCollect";//應回倉
	public final static String FIELD_LEAVE_WAREHOUSE_PACKAGES = "leaveWarehouseBox";//已理貨
	public final static String FIELD_LEAVE_WAREHOUSE_ORDER = "leaveWarehouse";//已理貨
	public final static String FIELD_TOTAL_LEAVE_WAREHOUSE_PACKAGES = "totalLeaveWarehouse";//應理貨
	public final static String FIELD_ARRIVED_HUB_PACKAGES = "arriveHub";//已達配送中心
	public final static String FIELD_TRANSFER_IN_PACKAGES = "transferIn";//應達配送中心
	public final static String FIELD_LEAVE_HUB_PACKAGES = "leaveHub";//已轉運裝車
	public final static String FIELD_TRANSFER_OUT_PACKAGES = "transferOut";//應轉運裝車
	public final static String FIELD_SUCCESS_DELIVERY_PACKAGES = "successDelivery";//配送完成
	public final static String FIELD_FAIL_DELIVERY_PACKAGES = "failDelivery";//配送異常
	public final static String FIELD_TOTAL_DELIVERY_PACKAGES = "totalDelivery";//配送
	public final static String FIELD_GOOD_CONDITION_PACKAGES = "goodConditionBox";//包裹完好
	public final static String FIELD_BAD_CONDITION_PACKAGES = "badConditionBox";//包裹異常
	public final static String FIELD_FINISH_RETURN_PACKAGES = "finishReturn";//已配後回倉
	public final static String FIELD_TOTAL_RETURN_PACKAGES = "totalReturn";//配後回倉
	public final static String FIELD_COLLECT_PACKAGES_DETAIL = "collectDetail";
	public final static String FIELD_LEAVE_WAREHOUSE_PACKAGES_DETAIL = "leaveWarehouseDetail";
	public final static String FIELD_TRANSFER_OUT_PACKAGES_DETAIL = "transferOutDetail";
	public final static String FIELD_TRANSFER_IN_PACKAGES_DETAIL = "transferInDetail";
	
	//time
	public final static String TRANSFER_IN_RESTART_TIME = "120000";
	public final static String TRANSFER_OUT_RESTART_TIME = "090000";
	
	public final static String[] FIELD_NAME_ARRAY = new String[] {
			FIELD_FINISH_COLLECT_PACKAGES,FIELD_COLLECT_PACKAGES,FIELD_LEAVE_WAREHOUSE_PACKAGES,FIELD_TOTAL_LEAVE_WAREHOUSE_PACKAGES,
			FIELD_ARRIVED_HUB_PACKAGES,FIELD_TRANSFER_IN_PACKAGES,FIELD_LEAVE_HUB_PACKAGES,FIELD_TRANSFER_OUT_PACKAGES,
			FIELD_SUCCESS_DELIVERY_PACKAGES,FIELD_FAIL_DELIVERY_PACKAGES,FIELD_TOTAL_DELIVERY_PACKAGES,FIELD_GOOD_CONDITION_PACKAGES,
			FIELD_BAD_CONDITION_PACKAGES,FIELD_TOTAL_RETURN_PACKAGES,FIELD_COLLECT_PACKAGES_DETAIL,FIELD_LEAVE_WAREHOUSE_PACKAGES_DETAIL,
			FIELD_TRANSFER_OUT_PACKAGES_DETAIL,};
	
}
