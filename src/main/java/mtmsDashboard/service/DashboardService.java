package mtmsDashboard.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jodd.util.ArraysUtil;
import mtmsDashboard.common.Pa;
import mtmsDashboard.dao.DashboardDao;
import mtmsDashboard.model.Dashboard;
import mtmsDashboard.model.Dc;
import mtmsDashboard.utils.DateUtils;

@Service
public class DashboardService {
	
	@Autowired
	DashboardDao dashboardDao;
	
	public List<Map<String, Object>> getDataBaseDcList() {
		List<Map<String, Object>> list = new ArrayList<>();
		List<Map<String, Object>> rs = dashboardDao.findDisName();
		for (Map<String, Object> m : rs) {
			Map<String, Object> map = new LinkedHashMap<>();
			map.put("id", m.get(Dc.FIELD_REGION_ID).toString());
			map.put("disName", m.get(Dc.FIELD_SHOW_DIS_NAME).toString());
			map.put("value", m.get(Dc.FIELD_DC_ID).toString());
			list.add(map);
		}
		return list;
	}

	public String getDcDisName(String dcId) {
		return dashboardDao.findDisName(dcId);
	}
	
	public Map<String,Object> warehouseTaskDashboardValue(String dc){
		Map<String,Object> rs = new HashMap<String,Object>();
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		String yesterday = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(-1), DateUtils.DATE_FORMAT_8);
		String tommorow = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(1), DateUtils.DATE_FORMAT_8);
		List<Map<String,Object>> dcList = getDataBaseDcList();
		
		//在倉發貨 time range = d-day 00 ~ updateTime, start flow 110, finish if flow = 220
		System.out.println("在倉發貨");
		String[] finishFlow = new String[] {Pa.DOC_FLOW_CLOSE_CREATE};
		String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		Map<String,Object> map = getLeaveWarehouseMap(dcList,"",dc,date,date+"000000",time,Pa.DOC_FLOW_INIT,finishFlow,excludeFlow);
		rs.put(Dashboard.FIELD_LEAVE_WAREHOUSE_PACKAGES, map.get("finishBox"));
		rs.put(Dashboard.FIELD_LEAVE_WAREHOUSE_ORDER, map.get("finish"));
		rs.put(Dashboard.FIELD_TOTAL_LEAVE_WAREHOUSE_PACKAGES, map.get("total"));
		rs.put(Dashboard.FIELD_LEAVE_WAREHOUSE_PACKAGES_DETAIL, map.get("detail"));
				
		//集貨回倉 time range = d-day 00 ~ updateTime, start flow 225, finish if flow = 235, exclude 在倉發貨
		System.out.println("集貨回倉");
		finishFlow = new String[] {Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
		String[] startFlow = new String[] {Pa.DOC_FLOW_COLLECTED};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY,Pa.DOC_FLOW_START_OUTBOUNT};
		map = getCollectMap(dcList,"",dc,date+"000000",time,startFlow,finishFlow,excludeFlow);
		rs.put(Dashboard.FIELD_FINISH_COLLECT_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_COLLECT_PACKAGES, map.get("total"));
		rs.put(Dashboard.FIELD_COLLECT_PACKAGES_DETAIL, map.get("detail"));
		
		//轉運裝車 time range = d-1 09 ~ d-day 08:59, start flow 235, finish flow 238, exclude sourceDc=targetDc' orders
		System.out.println("裝運裝車");
		finishFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP};
		startFlow = new String[] {Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		String st = yesterday+"000000";
		String et = yesterday+"240000";
		String finishSt = st;
		String finishEt = time;
		String transferRestartTime = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
		if(DateUtils.isAfter(time, transferRestartTime)) {
			st = date+"000000";
			et = time;
			finishSt = st;
			finishEt = tommorow+Dashboard.TRANSFER_OUT_RESTART_TIME;
		}
		map = getTransferMap(dcList, dc, "", dc, st, et, finishSt, finishEt, startFlow, finishFlow, excludeFlow);
		rs.put(Dashboard.FIELD_LEAVE_HUB_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_TRANSFER_OUT_PACKAGES,map.get("total"));
		rs.put(Dashboard.FIELD_TRANSFER_OUT_PACKAGES_DETAIL, map.get("detail"));
				
		//抵達配送中心 time range = d-1 12 ~ d-day 11:59, start flow 238,240, finish if flow = 400
		System.out.println("抵達配送中心");
		finishFlow = new String[] {Pa.DOC_FLOW_ARRIVE_END_DC};
		startFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP,Pa.DOC_FLOW_TRANSFER_DC_ING};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		st = yesterday+Dashboard.TRANSFER_OUT_RESTART_TIME;
		et = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
		finishSt = st;
		finishEt = time;
		 transferRestartTime = date+Dashboard.TRANSFER_IN_RESTART_TIME;
		if(DateUtils.isAfter(time, transferRestartTime)) {
			st = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
			et = time;
			finishSt = st;
			finishEt = tommorow+Dashboard.TRANSFER_IN_RESTART_TIME;
		}
		map = getTransferMap(dcList, dc, dc, "", st, et, finishSt, finishEt, startFlow, finishFlow, excludeFlow);
		rs.putAll(map);
		rs.put(Dashboard.FIELD_ARRIVED_HUB_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_TRANSFER_IN_PACKAGES, map.get("total"));
		rs.put(Dashboard.FIELD_TRANSFER_IN_PACKAGES_DETAIL, map.get("detail"));
			
		//配送  time range = d-day 00 ~ updateTime, start flow , order is in today's town router, finish if flow = 900 or 1000
		System.out.println("配送");
		map = getDeliveryMap(dc, date, time, new String[] {Pa.DOC_FLOW_POD},new String[] {Pa.DOC_FLOW_EXC});
		rs.put(Dashboard.FIELD_SUCCESS_DELIVERY_PACKAGES, map.get("success"));
		rs.put(Dashboard.FIELD_FAIL_DELIVERY_PACKAGES, map.get("fail"));
		rs.put(Dashboard.FIELD_TOTAL_DELIVERY_PACKAGES, map.get("total"));
				
		//配後回倉 time range = d-day 00 ~ updateTime, start flow = 1000, finish if flow = 1100,1150
		System.out.println("配後回倉");
		finishFlow = new String[] {Pa.DOC_FLOW_WMS_GET_GOOD_BOX,Pa.DOC_FLOW_WMS_GET_BAD_BOX};
		startFlow = new String[] {Pa.DOC_FLOW_EXC};
		excludeFlow = new String[] {Pa.DOC_FLOW_POD,Pa.DOC_FLOW_NO_DELIVERY};
		map = getReturnToWarehouseMap(dc, "", date, date+"000000", time, finishFlow, startFlow, excludeFlow);
		rs.put(Dashboard.FIELD_FINISH_RETURN_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_TOTAL_RETURN_PACKAGES, map.get("total"));
		return rs;
	}
	
	public Map<String,Object> getCollectMap(List<Map<String,Object>> dcList, String targetDc, String sourceDc, String st, String et, String[] startFlows, String[] finishFlows, String[] excludeFlows){
		Map<String,Object> rs = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		List<Map<String,Object>> list = dashboardDao.getCollectOrder(targetDc, sourceDc, st, et, startFlows, finishFlows, excludeFlows);
		int finish = 0;
		int total = 0;
		for(Map<String,Object> t:list) {
			if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
				finish+= Integer.parseInt(t.get("finishBox").toString());
			}
			total+= Integer.parseInt(t.get("totalBox").toString());
		}
		for(Map<String,Object> d:dcList) {
			int detailFinish = 0;
			int detailTotal = 0;
			double percent = 0;
			String bg = "#cccccc";
			for(Map<String,Object> t:list) {
				if(t.get("targetDcId").equals(d.get("value"))) {
					if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
						detailFinish+= Integer.parseInt(t.get("finishBox").toString());
					}
					detailTotal+= Integer.parseInt(t.get("totalBox").toString());
				}
			}
			if(detailTotal>0) {
				percent = ((double)detailFinish/detailTotal)*100.0; 
				if(percent==100) {
					bg="green";
				}else {
					bg="orange";
				}
			}
			sb.append("<span style='font-size:30px;text-align:left;margin:3px'>"+d.get("disName").toString()+" : </span>"
					+ "<span style='float:right;font-size:30px'>"+detailFinish+"/"+detailTotal+"</span><div class='line-gray'><div style='height:30px;width:"+percent+"%;background-color:"+bg+";display:inline-block;'></div>"
					+ "</div>");
		}
		
		rs.put("finish", finish);
		rs.put("total", total);
		rs.put("detail", sb);
		return rs;
	}
	
	public List<Map<String,Object>> groupTargetDcId(List<Map<String,Object>> rs){
		Map<String,Map<String,Object>> map = new HashMap<>();
		for(Map<String,Object> m:rs) {
			String targetDcId = String.valueOf(m.get("targetDcId"));
			if(map.containsKey(targetDcId)) {
				Map<String,Object> detailMap = map.get(targetDcId);
				int totalBox = Integer.parseInt(String.valueOf(detailMap.get("totalBox")));
				int finishBox = Integer.parseInt(String.valueOf(detailMap.get("finishBox")));
				totalBox += Integer.parseInt(String.valueOf(m.get("totalBox")==null?0:m.get("totalBox")));
				finishBox += Integer.parseInt(String.valueOf(m.get("finishBox")==null?0:m.get("finishBox")));
				detailMap.put("totalBox", totalBox);
				detailMap.put("finishBox", finishBox);
			}else {
				int totalBox = 0;
				int finishBox = 0;
				Map<String,Object> detailMap = new HashMap<>();
				totalBox += Integer.parseInt(String.valueOf(m.get("totalBox")==null?0:m.get("totalBox")));
				finishBox += Integer.parseInt(String.valueOf(m.get("finishBox")==null?0:m.get("finishBox")));
				detailMap.put("totalBox", totalBox);
				detailMap.put("finishBox", finishBox);
				detailMap.put("targetDcId", String.valueOf(m.get("targetDcId")));
				detailMap.put("sourceDcId", String.valueOf(m.get("sourceDcId")));
				map.put(targetDcId, detailMap);
			}
		}
		List<Map<String,Object>> list = new ArrayList<>();
		for(String key:map.keySet()) {
			list.add(map.get(key));
		}
		
		return list;
	}
	
	public Map<String,Object> getTransferMap(List<Map<String,Object>> dcList, String dc, String targetDc, String sourceDc, String st, String et, String finishSt, String finishEt, 
			String[] startFlows, String[] finishFlows, String[] excludeFlows){
		Map<String,Object> rs = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		List<Map<String,Object>> _rs = dashboardDao.getTransferOrderByFlowsAndTargetDc(targetDc, sourceDc, st, et, finishSt, finishEt, startFlows, finishFlows, excludeFlows);
		List<Map<String,Object>> list = groupTargetDcId(_rs);
		int finish = 0;
		int total = 0;
		
		//main map
		for(Map<String,Object> t:list) {
			if(!t.get("targetDcId").toString().equals(t.get("sourceDcId").toString())) {
				if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
					finish += Integer.parseInt(t.get("finishBox").toString());
				}
				total += Integer.parseInt(t.get("totalBox").toString());
			}
		}
		//detail map
		for(Map<String,Object> d:dcList) {
			if(!d.get("value").equals(dc)) {
				int detailFinish = 0;
				int detailTotal = 0;
				double percent = 0;
				String bg = "#cccccc";
				for(Map<String,Object> t:list) {
					if(!t.get("targetDcId").toString().equals(t.get("sourceDcId").toString())) {
						if(StringUtils.isNotBlank(sourceDc)) {//轉運裝車
							if(t.get("targetDcId").equals(d.get("value"))) {
								if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
									detailFinish+= Integer.parseInt(t.get("finishBox").toString());
								}
								detailTotal+= Integer.parseInt(t.get("totalBox").toString());
							}
						}
						if(StringUtils.isNotBlank(targetDc)) {//抵達配送中心
							if(t.get("sourceDcId").equals(d.get("value"))) {
								if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
									detailFinish+= Integer.parseInt(t.get("finishBox").toString());
								}
								detailTotal+= Integer.parseInt(t.get("totalBox").toString());
							}
						}
					}
				}
				if(detailTotal>0) {
					percent = ((double)detailFinish/detailTotal)*100.0; 
					if(percent==100) {
						bg="green";
					}else {
						bg="orange";
					}
				}
				sb.append("<span style='font-size:30px;text-align:left;margin:3px'>"+d.get("disName").toString()+" : </span>"
						+ "<span style='float:right;font-size:30px'>"+detailFinish+"/"+detailTotal+"</span><div class='line-gray'><div style='height:30px;width:"+percent+"%;background-color:"+bg+";display:inline-block;'></div>"
						+ "</div>");
			}
		}
		rs.put("finish", finish);
		rs.put("total", total);
		rs.put("detail", sb);
		return rs;
	}
	
	public Map<String,Object> getTransferInMap(List<Map<String,Object>> dcList, String dc, String targetDc, String sourceDc, String st, String et, String finishSt, String finishEt, 
			String pickUpSt, String pickUpEt, String[] startFlows, String[] finishFlows, String[] excludeFlows){
		Map<String,Object> rs = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		List<Map<String,Object>> list = dashboardDao.getTransferInOrderByFlowsAndTargetDc(targetDc, sourceDc, st, et, finishSt, finishEt, 
				pickUpSt, pickUpEt, startFlows, finishFlows, excludeFlows);
		int finish = 0;
		int total = 0;
		
		//main map
		for(Map<String,Object> t:list) {
			if(!t.get("targetDcId").toString().equals(t.get("sourceDcId").toString())) {
				if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
					finish += Integer.parseInt(t.get("finishBox").toString());
				}
				total += Integer.parseInt(t.get("totalBox").toString());
			}
		}
		//detail map
		for(Map<String,Object> d:dcList) {
			if(!d.get("value").equals(dc)) {
				int detailFinish = 0;
				int detailTotal = 0;
				double percent = 0;
				String bg = "#cccccc";
				for(Map<String,Object> t:list) {
					if(!t.get("targetDcId").toString().equals(t.get("sourceDcId").toString())) {
						if(StringUtils.isNotBlank(sourceDc)) {//轉運裝車
							if(t.get("targetDcId").equals(d.get("value"))) {
								if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
									detailFinish+= Integer.parseInt(t.get("finishBox").toString());
								}
								detailTotal+= Integer.parseInt(t.get("totalBox").toString());
							}
						}
						if(StringUtils.isNotBlank(targetDc)) {//抵達配送中心
							if(t.get("sourceDcId").equals(d.get("value"))) {
								if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
									detailFinish+= Integer.parseInt(t.get("finishBox").toString());
								}
								detailTotal+= Integer.parseInt(t.get("totalBox").toString());
							}
						}
					}
				}
				if(detailTotal>0) {
					percent = ((double)detailFinish/detailTotal)*100.0; 
					if(percent==100) {
						bg="green";
					}else {
						bg="orange";
					}
				}
				sb.append("<span style='font-size:30px;text-align:left;margin:3px'>"+d.get("disName").toString()+" : </span>"
						+ "<span style='float:right;font-size:30px'>"+detailFinish+"/"+detailTotal+"</span><div class='line-gray'><div style='height:30px;width:"+percent+"%;background-color:"+bg+";display:inline-block;'></div>"
						+ "</div>");
			}
		}
		rs.put("finish", finish);
		rs.put("total", total);
		rs.put("detail", sb);
		return rs;
	}
	
	public Map<String,Object> getLeaveWarehouseMap(List<Map<String,Object>> dcList, String targetDc, String sourceDc, String date, String st, String et, String startFlow, String[] finishFlows, String[] excludeFlows){
		Map<String,Object> rs = new HashMap<String,Object>();
		StringBuilder sb = new StringBuilder();
		List<Map<String,Object>> list = dashboardDao.getLeaveWarehouseOrderByFlowsAndTargetDc(targetDc, sourceDc, date, st, et, startFlow, finishFlows, excludeFlows);
		int finish = 0 ;
		int finishBox = 0;
		int total = 0;
		for(Map<String,Object> t:list) {
			if(t.containsKey("finishBox") && t.get("finishBox")!=null) {
				finishBox+=Integer.parseInt(t.get("finishBox").toString());
			}
			finish+= Integer.parseInt(t.get("finishOrder").toString());
			total+= Integer.parseInt(t.get("totalOrder").toString());
		}
		for(Map<String,Object> d:dcList) {
			int detailFinish = 0;
			int detailTotal = 0;
			double percent = 0;
			String bg = "#cccccc";
			for(Map<String,Object> t:list) {
				if(t.get("targetDcId").equals(d.get("value"))) {
					detailFinish+= Integer.parseInt(t.get("finishOrder").toString());
					detailTotal+= Integer.parseInt(t.get("totalOrder").toString());
				}
			}
			if(detailTotal>0) {
				percent = ((double)detailFinish/detailTotal)*100; 
				if(percent==100) {
					bg="green";
				}else {
					bg="orange";
				}
			}
			sb.append("<span style='font-size:30px;text-align:left;margin:3px'>"+d.get("disName").toString()+" : </span>"
					+ "<span style='float:right;font-size:30px'>"+detailFinish+"/"+detailTotal+"</span><div class='line-gray'><div style='height:30px;width:"+percent+"%;background-color:"
					+bg+";display:inline-block;'></div>"
					+ "</div>");
		}
		rs.put("finishBox", finishBox);
		rs.put("finish", finish);
		rs.put("total", total);
		rs.put("detail", sb);
		return rs;
	}
	
	public Map<String,Object> getDeliveryMap(String targetDc, String shipDate, String et, String[] successFlows, String[] failFlows){
		Map<String,Object> rs = new HashMap<String,Object>();
//		List<Map<String,Object>> list = dashboardDao.getDeliveryOderByFlowsAndTargetDc(targetDc, sourceDc, st, et, startFlows, ArraysUtil.join(successFlows,failFlows) , excludeFlows);
		List<Map<String,Object>> list = dashboardDao.getDeliveryOrderByFlowsAndTargetDc(targetDc, shipDate);
		int success = 0;
		int fail = 0;
		int total = 0;
		List<Object> idList = new ArrayList<Object>();
		for(Map<String,Object> l:list) {
			if(l.containsKey("ecDocId")&&l.get("ecDocId")!=null) {
				idList.add(l.get("ecDocId"));
			}
			if(l.containsKey("boxQty")&&l.get("boxQty")!=null) {
				total += Integer.parseInt(String.valueOf(l.get("boxQty")));
			}
		}
		if(total>0) {
			List<Map<String,Object>> finish = dashboardDao.getFinishDeliveryOrderByFlowsAndTargetDc(shipDate+"000000", et, idList.toArray(), ArraysUtil.join(successFlows,failFlows));
			for(Map<String,Object> t:finish) {
				if(t.get("flowEnd")==null) {
					continue;
				}else if(ArrayUtils.contains(successFlows, t.get("flowEnd").toString())) {
					success += Integer.parseInt(t.get("totalBox").toString());
				}else if(ArrayUtils.contains(failFlows, t.get("flowEnd").toString())) {
					fail += Integer.parseInt(t.get("totalBox").toString());
				}
			}
		}
		
		rs.put("success", success);
		rs.put("fail", fail);
		rs.put("total", total);
		return rs;
	}
	
	public Map<String,Object> getReturnToWarehouseMap(String targetDc, String sourceDc, String shipDate, String st, String et, String[] finishFlows, String[] startFlows, String[] excludeFlows ){
		Map<String,Object> rs = new HashMap<String,Object>();
		List<Map<String,Object>> list = dashboardDao.getReturnOrder(targetDc, shipDate, st, et, startFlows, excludeFlows);
		int finish = 0;
		int total = 0;
		List<Object> idList = new ArrayList<Object>();
		for(Map<String,Object> l:list) {
			if(l.containsKey("ecDocId")&&l.get("ecDocId")!=null) {
				idList.add(l.get("ecDocId"));
			}
			if(l.containsKey("boxQty")&&l.get("boxQty")!=null) {
				total += Integer.parseInt(String.valueOf(l.get("boxQty")));
			}
		}
		if(total>0) {
			List<Map<String,Object>> finishList = dashboardDao.getFinishDeliveryOrderByFlowsAndTargetDc(shipDate+"000000", et, idList.toArray(), finishFlows);
			for(Map<String,Object> t:finishList) {
				if(t.get("flowEnd")==null) {
					continue;
				}else if(ArrayUtils.contains(finishFlows, t.get("flowEnd").toString())) {
					finish += Integer.parseInt(t.get("totalBox").toString());
				}
			}
		}
		
		rs.put("finish", finish);
		rs.put("total", total);
		return rs;
	}
	
	public Object[] getOrderIdSet(List<Map<String,Object>> list){
		List<String> rs = new ArrayList<String>();
		for(Map<String,Object> l:list) {
			if(l.containsKey("ecDocId")&&l.get("ecDocId")!=null) {
				rs.add(l.get("ecDocId").toString());
			}
		}
		return rs.toArray();
	}
	
	public Map<String,Object> dashboardTest(String dc, String date){
		Map<String,Object> rs = new HashMap<String,Object>();
		String now = DateUtils.getNowTimeString();
		
		date = DateUtils.toString(DateUtils.parseStringToDate(date, DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(date+"235959", DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		String yesterday = DateUtils.toString(DateUtils.parseStringToDate(date, DateUtils.DATE_FORMAT_8).plusDays(-1), DateUtils.DATE_FORMAT_8);
		String tommorow = DateUtils.toString(DateUtils.parseStringToDate(date, DateUtils.DATE_FORMAT_8).plusDays(1), DateUtils.DATE_FORMAT_8);
		List<Map<String,Object>> dcList = getDataBaseDcList();
		
		//在倉發貨 time range = d-day 00 ~ updateTime, start flow 110, finish if flow = 220
		System.out.println("在倉發貨");
		String[] finishFlow = new String[] {Pa.DOC_FLOW_CLOSE_CREATE};
		String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		Map<String,Object> map = getLeaveWarehouseMap(dcList,"",dc,date,date+"000000",time,Pa.DOC_FLOW_INIT,finishFlow,excludeFlow);
		rs.putAll(map);
		
		//集貨回倉 time range = d-day 00 ~ updateTime, start flow 225, finish if flow = 235, exclude 在倉發貨
		System.out.println("集貨回倉");
		finishFlow = new String[] {Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
		String[] startFlow = new String[] {Pa.DOC_FLOW_COLLECTED};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY,Pa.DOC_FLOW_START_OUTBOUNT};
		map = getCollectMap(dcList,"",dc,date+"000000",time,startFlow,finishFlow,excludeFlow);
		rs.put(Dashboard.FIELD_FINISH_COLLECT_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_COLLECT_PACKAGES, map.get("total"));
		rs.put(Dashboard.FIELD_COLLECT_PACKAGES_DETAIL, map.get("detail"));
		
		//轉運裝車 time range = d-1 09 ~ d-day 08:59, start flow 235, finish flow 238, exclude sourceDc=targetDc' orders
		System.out.println("裝運裝車");
		finishFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP};
		startFlow = new String[] {Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		String st = yesterday+"000000";
		String et = yesterday+"240000";
		String finishSt = st;
		String finishEt = time;
		String transferRestartTime = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
		if(DateUtils.isAfter(time, transferRestartTime)) {
			st = date+"000000";
			et = time;
			finishSt = st;
			finishEt = tommorow+Dashboard.TRANSFER_OUT_RESTART_TIME;
		}
		map = getTransferMap(dcList, dc, "", dc, st, et, finishSt, finishEt, startFlow, finishFlow, excludeFlow);
		rs.put(Dashboard.FIELD_LEAVE_HUB_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_TRANSFER_OUT_PACKAGES,map.get("total"));
		rs.put(Dashboard.FIELD_TRANSFER_OUT_PACKAGES_DETAIL, map.get("detail"));
						
		//抵達配送中心 time range = d-1 12 ~ d-day 11:59, start flow 238,240, finish if flow = 400
		System.out.println("抵達配送中心");
		finishFlow = new String[] {Pa.DOC_FLOW_ARRIVE_END_DC};
		startFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP,Pa.DOC_FLOW_TRANSFER_DC_ING};
		excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
		st = yesterday+Dashboard.TRANSFER_OUT_RESTART_TIME;
		et = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
		finishSt = st;
		finishEt = time;
		 transferRestartTime = date+Dashboard.TRANSFER_IN_RESTART_TIME;
		if(DateUtils.isAfter(time, transferRestartTime)) {
			st = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
			et = time;
			finishSt = st;
			finishEt = tommorow+Dashboard.TRANSFER_IN_RESTART_TIME;
		}
		map = getTransferMap(dcList, dc, dc, "", st, et, finishSt, finishEt, startFlow, finishFlow, excludeFlow);
		rs.putAll(map);
		rs.put(Dashboard.FIELD_ARRIVED_HUB_PACKAGES, map.get("finish"));
		rs.put(Dashboard.FIELD_TRANSFER_IN_PACKAGES, map.get("total"));
		rs.put(Dashboard.FIELD_TRANSFER_IN_PACKAGES_DETAIL, map.get("detail"));
		
		//配送  time range = d-day 00 ~ updateTime, start flow , order is in today's town router, finish if flow = 900 or 1000
		System.out.println("配送");
		map = getDeliveryMap(dc, date, time, new String[] {Pa.DOC_FLOW_POD},new String[] {Pa.DOC_FLOW_EXC});
		rs.putAll(map);
		
		//配後回倉 time range = d-day 00 ~ updateTime, start flow = 1000, finish if flow = 1100,1150
		System.out.println("配後回倉");
		finishFlow = new String[] {Pa.DOC_FLOW_WMS_GET_GOOD_BOX,Pa.DOC_FLOW_WMS_GET_BAD_BOX};
		startFlow = new String[] {Pa.DOC_FLOW_EXC};
		excludeFlow = new String[] {Pa.DOC_FLOW_POD,Pa.DOC_FLOW_NO_DELIVERY};
		map = getReturnToWarehouseMap(dc, "", date, date+"000000", time, finishFlow, startFlow, excludeFlow);
		rs.putAll(map);
		return rs;
	}

}
