package mtmsDashboard.bean;


import javax.sql.DataSource;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.zaxxer.hikari.HikariDataSource;

import mtmsDashboard.common.Pa;

@Configuration
public class DataSourceConnection {
	
	@Bean(name = "dataSource")
	public static DataSource getDataSource() {		
		
		HikariDataSource data = new HikariDataSource();
		data.setJdbcUrl("jdbc:mysql://"+Pa.DB+"/"+Pa.DB_NAME+"?useUnicode=true&characterEncoding=UTF-8");
		data.setDriverClassName("com.mysql.cj.jdbc.Driver");
		data.setUsername(Pa.DB_USER);
		data.setPassword(Pa.DB_PWD);
		data.setMaximumPoolSize(16);
		
		return data;
	}
}
