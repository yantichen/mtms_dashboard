package mtmsDashboard.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mtmsDashboard.common.Pa;
import mtmsDashboard.model.Dashboard;
import mtmsDashboard.service.DashboardService;
import mtmsDashboard.utils.DateUtils;


@Controller
public class DashboardController {
	
	@Autowired
	DashboardService dashboardService;
	
	
	@GetMapping(value="/")
	public String mainPage(ModelMap model) {
		List<Map<String,Object>> dcList = dashboardService.getDataBaseDcList();
		StringBuilder sb = new StringBuilder();
		for(Map<String,Object> m:dcList) {
//			sb.append("<form style='display:inline' action='./warehouseDashboard/?dc="+m.get("value").toString()+"' method = 'GET'><input class='dc-btn' style='width:18%' type='submit' value='"+m.get("disName").toString()+"' ></form>");
			sb.append("<a href='./warehouseDashboard/?dc="+m.get("value").toString()+"'><button>"+m.get("disName").toString()+"</button></a>");
			model.addAttribute("dcInput",sb);
		}
		return "mainPageView";
	}
	
	@GetMapping(value="/warehouseDashboard")
	public String warehouseDisplay(
			@RequestParam(value="dc",defaultValue="") String dc,
			ModelMap model) {
//		model.addAllAttributes(dashboardService.warehouseTaskDashboardValue(dc));
		
		StringBuilder sb = new StringBuilder();
		sb.append("<label style='display:inline'>"+dashboardService.getDcDisName(dc)+"</label>");
		model.addAttribute("dcDisName", sb);
		model.addAttribute("time", DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14));
		model.addAttribute("dc", dc);
		return "warehouseDashboardView";
	}
	
	@GetMapping(value="/date")
	public @ResponseBody String date() {
		return DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.TIME_FORMAT);
	}
	
	@GetMapping(value="/warehouseDashboardTest")
	public String warehouseTest(
			@RequestParam(value="dc",defaultValue="") String dc,
			@RequestParam(value="date",defaultValue="") String date,
			ModelMap model) {
		model.addAllAttributes(dashboardService.dashboardTest(dc,date));
		
		StringBuilder sb = new StringBuilder();
		sb.append("<label style='display:inline'>"+dashboardService.getDcDisName(dc)+"</label>");
		model.addAttribute("dcDisName", sb);
		model.addAttribute("time", DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14));
		model.addAttribute("dc", dc);
		return "warehouseDashboardView";
	}
	
	@RequestMapping(value="/warehouseOutbound" ,method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public @ResponseBody Map<String,Object> warehouseOutboundMap(@RequestParam(value="dc",defaultValue="") String dc){
		//在倉發貨 time range = d-day 00 ~ updateTime, start flow 110, finish if flow = 220
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		List<Map<String,Object>> dcList = dashboardService.getDataBaseDcList();
		
		try {
			String[] finishFlow = new String[] {Pa.DOC_FLOW_CLOSE_CREATE};
			String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
			Map<String,Object> map = dashboardService.getLeaveWarehouseMap(dcList,"",dc,date,date+"000000",time,Pa.DOC_FLOW_START_OUTBOUNT,finishFlow,excludeFlow);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	@GetMapping(value="/collectOrder")
	public @ResponseBody Map<String,Object> collectOrderMap(@RequestParam(value="dc",defaultValue="") String dc){
		//集貨回倉 time range = d-day 00 ~ updateTime, start flow 225, finish if flow = 235, exclude 在倉發貨
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		List<Map<String,Object>> dcList = dashboardService.getDataBaseDcList();
		
		try {
			String[] finishFlow = new String[] {Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
			String[] startFlow = new String[] {Pa.DOC_FLOW_COLLECTED};
			String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY,Pa.DOC_FLOW_START_OUTBOUNT};
			Map<String,Object> map = dashboardService.getCollectMap(dcList,"",dc,date+"000000",time,startFlow,finishFlow,excludeFlow);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	@GetMapping(value="/transferOut")
	public  @ResponseBody Map<String,Object> transferOutMap(@RequestParam(value="dc",defaultValue="") String dc){
		//轉運裝車 time range = d-1 09 ~ d-day 08:59, start flow 235, finish flow 238, exclude sourceDc=targetDc' orders
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		String yesterday = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(-1), DateUtils.DATE_FORMAT_8);
		String tommorow = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(1), DateUtils.DATE_FORMAT_8);
		List<Map<String,Object>> dcList = dashboardService.getDataBaseDcList();
		
		try {
			String[] finishFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP};
			String[] startFlow = new String[] {Pa.DOC_FLOW_COLLECTED, Pa.DOC_FLOW_COLLECTED_BY_WMS_APP};
			String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
			String st = yesterday+"000000";
			String et = yesterday+"240000";
			String finishSt = st;
			String finishEt = time;
			String transferRestartTime = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
			if(DateUtils.isAfter(time, transferRestartTime)) {
				st = date+"000000";
				et = time;
				finishSt = st;
				finishEt = tommorow+Dashboard.TRANSFER_OUT_RESTART_TIME;
			}
			Map<String,Object> map = dashboardService.getTransferMap(dcList, dc, "", dc, st, et, finishSt, finishEt, startFlow, finishFlow, excludeFlow);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	@GetMapping(value="/transferIn")
	public  @ResponseBody Map<String,Object> transferInMap(@RequestParam(value="dc",defaultValue="") String dc){
		//抵達配送中心 time range = d-1 12 ~ d-day 11:59, start flow 238,240, finish if flow = 400
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		String yesterday = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(-1), DateUtils.DATE_FORMAT_8);
		String tommorow = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8).plusDays(1), DateUtils.DATE_FORMAT_8);
		List<Map<String,Object>> dcList = dashboardService.getDataBaseDcList();
		
		try {
			String[] finishFlow = new String[] {Pa.DOC_FLOW_ARRIVE_END_DC};
			String[] startFlow = new String[] {Pa.DOC_FLOW_PUT_TO_CONTAINER_BY_WMS_APP,Pa.DOC_FLOW_TRANSFER_DC_ING};
			String[] excludeFlow = new String[] {Pa.DOC_FLOW_NO_DELIVERY};
			String st = yesterday+Dashboard.TRANSFER_OUT_RESTART_TIME;
			String et = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
			String finishSt = st;
			String finishEt = time;
			String pickUpSt = yesterday+"000000";
			String pickUpEt = yesterday+"235959";
			String transferRestartTime = date+Dashboard.TRANSFER_IN_RESTART_TIME;
			if(DateUtils.isAfter(time, transferRestartTime)) {
				st = date+Dashboard.TRANSFER_OUT_RESTART_TIME;
				et = time;
				pickUpSt = date+"000000";
				pickUpEt = date+"235959";
				finishSt = st;
				finishEt = tommorow+Dashboard.TRANSFER_IN_RESTART_TIME;
			}
			Map<String,Object> map = dashboardService.getTransferInMap(dcList, dc, dc, "", st, et, finishSt, finishEt, pickUpSt, pickUpEt, startFlow, finishFlow, excludeFlow);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	@GetMapping(value="/delivery")
	public  @ResponseBody Map<String,Object> deliveryMap(@RequestParam(value="dc",defaultValue="") String dc){
		//配送  time range = d-day 00 ~ updateTime, start flow , order is in today's town router, finish if flow = 900 or 1000
		String[] successFlows = new String[] {Pa.DOC_FLOW_POD};
		String[] failFlows = new String[] {Pa.DOC_FLOW_EXC};
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		try {
			Map<String,Object> map = dashboardService.getDeliveryMap(dc, date, time, successFlows, failFlows);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	@GetMapping(value="/returnWarehouse")
	public @ResponseBody Map<String,Object> returnWarehouseMap(@RequestParam(value="dc",defaultValue="") String dc){
		//配後回倉 time range = d-day 00 ~ updateTime, start flow = 1000, finish if flow = 1100,1150
		String[] finishFlow = new String[] {Pa.DOC_FLOW_WMS_GET_GOOD_BOX,Pa.DOC_FLOW_WMS_GET_BAD_BOX};
		String[] startFlow = new String[] {Pa.DOC_FLOW_EXC};
		String[] excludeFlow = new String[] {Pa.DOC_FLOW_POD,Pa.DOC_FLOW_NO_DELIVERY};
		String date = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getTodayString(), DateUtils.DATE_FORMAT_8), DateUtils.DATE_FORMAT_8);
		String time = DateUtils.toString(DateUtils.parseStringToDate(DateUtils.getNowTimeString(), DateUtils.DATE_TIME_FORMAT_14), DateUtils.DATE_TIME_FORMAT_14);
		
		try {
			Map<String,Object> map = dashboardService.getReturnToWarehouseMap(dc, "", date, date+"000000", time, finishFlow, startFlow, excludeFlow);
			return map;
		}catch(Exception e) {
			e.printStackTrace();
			return new HashMap<String,Object>();
		}
	}
	
	
}
